# Boards

Boards is a project organizer application that offers kanban boards for GNOME.
Boards is inspired by the experience of Trello project.

## What is Kanban Boards?

A kanban board is one of the tools that can be used to implement kanban to manage
work at a personal or organizational level.
Kanban boards visually depict work at various stages of a process using cards to
represent work items and columns to represent each stage of the process.
Cards are moved from left to right to show progress and to help coordinate teams
performing the work.
A kanban board may be divided into horizontal "swimlanes" representing different
kinds of work or different teams performing the work.
Kanban boards can be used in knowledge work or for manufacturing processes.
Simple boards have columns for "waiting", "in progress" and "completed" or
"to-do", "doing", and "done". Complex kanban boards can be created that subdivide
"in progress" work into multiple columns to visualise the flow of work across a
whole value stream map.
From a [Wikipedia Article](https://en.wikipedia.org/wiki/Kanban_board)

## Dependencies

 * GTK 4.6 or newer
 * Libadwaita
 * Libpanel: https://gitlab.gnome.org/chergert/libpanel.git

Refer to the [io.sam.Boards.json](https://gitlab.gnome.org/smonder/Boards/blob/master/io.sam.Boards.json) Flatpak manifest for additional details.

## Try it Out!

### Building

We use the Meson (and thereby Ninja) build system for Boards. The quickest
way to get going is to do the following:

```sh
meson . ./build
ninja -C ./build
ninja -C ./build install
```

For build options see [meson_options.txt](./meson_options.txt).
/* boards-window-actions.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "boards-window"

#include "config.h"
#include "boards-debug.h"

#include "boards-window-private.h"


static void
boards_window_close_surface_action (GSimpleAction *action,
                                    GVariant      *parameter,
                                    gpointer       user_data)
{
  BoardsWindow *self = user_data;

  g_assert (BOARDS_IS_WINDOW (self));

  BOARDS_ENTRY;
  boards_surface_unset (self->current_surface);
  gtk_stack_remove (self->surfaces, GTK_WIDGET (self->current_surface));
  g_clear_weak_pointer (&self->current_surface);

  gtk_stack_set_visible_child (self->surfaces, GTK_WIDGET (self->greeter));
  g_set_weak_pointer (&self->current_surface, self->greeter);
  boards_omnibar_set_page (self->omnibar, NULL);
  gtk_window_set_title (GTK_WINDOW (self), PACKAGE_NAME);
  gtk_widget_set_visible (GTK_WIDGET (self->close_page), FALSE);

  BOARDS_EXIT;
}

static GActionEntry window_actions [] =
  {
    { "close-surface", boards_window_close_surface_action },
  };


void
_boards_window_init_actions (BoardsWindow *self)
{
  g_autoptr (GSettings) settings = g_settings_new ("io.sam.Boards");
  g_autoptr (GAction) style_action = g_settings_create_action (settings, "style-variant");

  GtkPopover *popover;
  GMenu *menu;

  g_return_if_fail (BOARDS_IS_WINDOW (self));

  BOARDS_ENTRY;
  menu = gtk_application_get_menu_by_id (GTK_APPLICATION (BOARDS_APPLICATION_DEFAULT), "primary_menu");
  gtk_menu_button_set_menu_model (self->menu_button, G_MENU_MODEL (menu));

  popover = gtk_menu_button_get_popover (self->menu_button);
  gtk_popover_menu_add_child (GTK_POPOVER_MENU (popover),
                              g_object_new (PANEL_TYPE_THEME_SELECTOR,
                                            "action-name", "app.style-variant",
                                            NULL),
                              "theme");

  g_action_map_add_action (G_ACTION_MAP (self), style_action);

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   window_actions,
                                   G_N_ELEMENTS (window_actions),
                                   self);
  BOARDS_EXIT;
}

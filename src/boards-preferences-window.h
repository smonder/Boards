/* boards-preferences-window.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <adwaita.h>
#include "boards-version-macros.h"

G_BEGIN_DECLS

#define BOARDS_TYPE_PREFERENCES_WINDOW (boards_preferences_window_get_type())

BOARDS_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (BoardsPreferencesWindow, boards_preferences_window, BOARDS, PREFERENCES_WINDOW, AdwPreferencesWindow)

BOARDS_AVAILABLE_IN_ALL
GtkWindow * boards_preferences_window_new   (void) G_GNUC_WARN_UNUSED_RESULT;

G_END_DECLS

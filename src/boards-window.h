/* boards-window.h
 *
 * Copyright 2022 Salim Monder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <adwaita.h>

#include "boards-surface.h"
#include "boards-version-macros.h"

G_BEGIN_DECLS

#define BOARDS_TYPE_WINDOW (boards_window_get_type())

BOARDS_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (BoardsWindow, boards_window, BOARDS, WINDOW, AdwApplicationWindow)

BOARDS_AVAILABLE_IN_ALL
BoardsWindow *  boards_window_new         (void) G_GNUC_WARN_UNUSED_RESULT;

BOARDS_AVAILABLE_IN_ALL
void            boards_window_add_surface (BoardsWindow *  self,
                                           BoardsSurface * surface);
BOARDS_AVAILABLE_IN_ALL
BoardsSurface * boards_window_get_surface (BoardsWindow *  self,
                                           const gchar *   surface_name);
G_END_DECLS

/* boards-application-actions.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "boards-application-actions"

#include "build-ident.h"
#include "config.h"
#include "boards-debug.h"

#include "boards-application-private.h"
#include "boards-preferences-window.h"


static void
boards_application_preferences (GSimpleAction *action,
                                GVariant      *parameter,
                                gpointer       user_data)
{
  BoardsApplication *self = user_data;
  GtkWindow *win;

  g_assert (BOARDS_IS_APPLICATION (self));
  g_assert (G_IS_SIMPLE_ACTION (action));

  win = boards_preferences_window_new ();
  gtk_window_set_application (win, GTK_APPLICATION (self));
  gtk_window_set_transient_for (win,
                                (GtkWindow *)boards_application_get_current_window (self));
  gtk_window_present (win);
}

static void
boards_application_show_about (GSimpleAction *action,
                               GVariant      *parameter,
                               gpointer       user_data)
{
  static const char *designers[] = {
    "Salim Monder",
    NULL
  };
  static const char *developers[] = {
    "Salim Monder",
    NULL
  };
  static const char *documenters[] = {
    "Salim Monder",
    NULL
  };

  BoardsApplication *self = user_data;

  g_assert (BOARDS_IS_APPLICATION (self));
  g_assert (G_IS_SIMPLE_ACTION (action));

  adw_show_about_window (GTK_WINDOW (boards_application_get_current_window (self)),
                         "application-name", PACKAGE_NAME,
                         "application-icon", "io.sam.Boards",
                         "version", BOARDS_BUILD_IDENTIFIER,
                         "copyright", "© 2022 Salim Monder",
                         "issue-url", "https://gitlab.gnome.org/smonder/Boards/issues/new",
                         "license-type", GTK_LICENSE_LGPL_3_0,
                         "developer-name", "Salim Monder",
                         "developers", developers,
                         "designers", designers,
                         "documenters", documenters,
                         "translator-credits", _("translator-credits"),
                         NULL);
}

static GActionEntry action_entries[] = {
  { "preferences", boards_application_preferences },
  { "about",       boards_application_show_about },
};


void
_boards_application_init_actions (BoardsApplication *self)
{
  g_return_if_fail (BOARDS_IS_APPLICATION (self));

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   action_entries, G_N_ELEMENTS (action_entries),
                                   self);
}


/* boards-application.c
 *
 * Copyright 2022 Salim Monder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "boards-application"

#include "config.h"
#include "boards-debug.h"

#include "boards-application-private.h"

G_DEFINE_TYPE (BoardsApplication, boards_application, ADW_TYPE_APPLICATION)

static gboolean
style_variant_to_color_scheme (GValue   *value,
                               GVariant *variant,
                               gpointer  user_data)
{
  const gchar *str = g_variant_get_string (variant, NULL);

  if (boards_str_equal (str, "dark"))
    g_value_set_enum (value, ADW_COLOR_SCHEME_FORCE_DARK);
  else if (boards_str_equal (str, "light"))
    g_value_set_enum (value, ADW_COLOR_SCHEME_FORCE_LIGHT);
  else
    g_value_set_enum (value, ADW_COLOR_SCHEME_DEFAULT);

  return TRUE;
}

static void
boards_application_startup (GApplication *app)
{
  BoardsApplication *self = (BoardsApplication *)app;
  AdwStyleManager *style_manager;
  g_autoptr(GtkCssProvider) css = NULL;
  g_autoptr(GAction) theme = NULL;

  g_assert (BOARDS_IS_APPLICATION (app));

  self->settings = g_settings_new ("io.sam.Boards");

  panel_init ();

  theme = g_settings_create_action (self->settings, "style-variant");
  g_action_map_add_action (G_ACTION_MAP (self), theme);

  G_APPLICATION_CLASS (boards_application_parent_class)->startup (app);

  css = gtk_css_provider_new ();
  gtk_css_provider_load_from_resource (css, "/css/stylesheet.css");
  gtk_style_context_add_provider_for_display (gdk_display_get_default (),
                                              GTK_STYLE_PROVIDER (css),
                                              GTK_STYLE_PROVIDER_PRIORITY_THEME+1);

  style_manager = adw_style_manager_get_default ();
  g_settings_bind_with_mapping (self->settings, "style-variant",
                                style_manager, "color-scheme",
                                G_SETTINGS_BIND_GET,
                                style_variant_to_color_scheme,
                                NULL, NULL, NULL);
}

static void
boards_application_shutdown (GApplication *app)
{
  BoardsApplication *self = (BoardsApplication *)app;

  g_assert (BOARDS_IS_APPLICATION (app));

  G_APPLICATION_CLASS (boards_application_parent_class)->shutdown (app);

  panel_finalize ();

  g_clear_object (&self->settings);
}

static void
boards_application_activate (GApplication *app)
{
  GtkWindow *window;

  /* It's good practice to check your parameters at the beginning of the
   * function. It helps catch errors early and in development instead of
   * by your users.
   */
  g_assert (GTK_IS_APPLICATION (app));

  /* Get the current window or create one if necessary. */
  window = gtk_application_get_active_window (GTK_APPLICATION (app));
  if (window == NULL)
    window = (GtkWindow *)boards_window_new ();

  /* Ask the window manager/compositor to present the window. */
  gtk_window_present (window);
}

static void
boards_application_window_added (GtkApplication *app,
                                 GtkWindow      *window)
{
  g_assert (ADW_IS_APPLICATION (app));
  g_assert (GTK_IS_WINDOW (window));

#if DEVELOPMENT_BUILD
  gtk_widget_add_css_class (GTK_WIDGET (window), "devel");
#endif

  GTK_APPLICATION_CLASS (boards_application_parent_class)->window_added (app, window);
}

static void
boards_application_class_init (BoardsApplicationClass *klass)
{
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);
  GtkApplicationClass *gtk_app_class = GTK_APPLICATION_CLASS (klass);

  app_class->activate = boards_application_activate;
  app_class->shutdown = boards_application_shutdown;
  app_class->startup = boards_application_startup;

  gtk_app_class->window_added = boards_application_window_added;
}

static const GOptionEntry entries[] = {
  { "standalone", 's', 0, G_OPTION_ARG_NONE, NULL, N_("Run a new instance of Boards") },
  { "version", 0, 0, G_OPTION_ARG_NONE, NULL, N_("Print version information and exit") },
  { 0 }
};

static void
boards_application_init (BoardsApplication *self)
{
  _boards_application_init_actions (self);

  g_application_add_main_option_entries (G_APPLICATION (self), entries);
}


/**
 * boards_application_query_recent:
 * @self: a #BoardsApplication instance.
 * @error: a #GError.
 *
 * Query the recent boards added and saved.
 * if error is occured, @error will be set and unexpected results will be returned.
 * TODO: This function needs further modification and optimization.
 *
 * Returns: (transfer full) (nullable): a #GListModel.
 *
 * Since: 0.1
 */
GListModel *
boards_application_query_recent (BoardsApplication  *self,
                                 GError            **error)
{
  g_autoptr (GListStore) boards_list_view = NULL;
  gchar *pages_dir_path;
  GDir *pages_dir;
  gchar *file_name;

  g_return_val_if_fail (BOARDS_IS_APPLICATION (self), NULL);

  BOARDS_ENTRY;

  boards_list_view = g_list_store_new (BOARDS_TYPE_PAGE);
  pages_dir_path = g_settings_get_string (self->settings, "boards-location");

  if (boards_str_equal (pages_dir_path, "(NONE)") || pages_dir_path == NULL)
    {
      g_free (pages_dir_path);
      pages_dir_path = g_build_filename (g_get_user_cache_dir (), "Boards", NULL);
    }

  pages_dir = g_dir_open (pages_dir_path, 0, error);

  if (!pages_dir)
    BOARDS_RETURN (NULL);

  while ((file_name = (gchar *)g_dir_read_name (pages_dir)) != NULL)
    {
      g_autoptr (BoardsPage) new_board = NULL;
      const gchar *file_path;

      if (!g_str_has_suffix (file_name, ".board"))
        continue;

      new_board = boards_page_new ();
      file_path = g_build_filename (pages_dir_path, file_name, NULL);

      if (boards_page_load_from_file (new_board,
                                       g_file_new_for_path (file_path),
                                       NULL, error))
        {
          g_list_store_append (boards_list_view, new_board);
          boards_page_set_file (new_board, g_file_new_for_path (file_path));
        }
    }

  g_dir_close (pages_dir);
  BOARDS_RETURN ((GListModel *)g_steal_pointer (&boards_list_view));
}

/**
 * boards_application_get_current_window:
 *
 * Gets the current #BoardsWindow.
 *
 * Returns: (transfer none) (nullable): a #BoardsWindow or %NULL.
 *
 * Since: 0.1
 */
BoardsWindow *
boards_application_get_current_window (BoardsApplication *self)
{
  const GList *windows;

  g_return_val_if_fail (BOARDS_IS_APPLICATION (self), NULL);

  windows = gtk_application_get_windows (GTK_APPLICATION (self));

  for (const GList *iter = windows; iter; iter = iter->next)
    if (BOARDS_IS_WINDOW (iter->data))
      return BOARDS_WINDOW (iter->data);

  return NULL;
}


/**
 * _boards_application_new:
 * @standalone: a #gboolean: If we need to run a new instance.
 *
 * Creates a new #BoardsApplication instance.
 *
 * Returns: a newly created #BoardsApplication.
 *
 * Since: 0.1
 */
BoardsApplication *
_boards_application_new (gboolean standalone)
{
  BoardsApplication *app;
  GApplicationFlags flags = G_APPLICATION_FLAGS_NONE;

  if (standalone)
    flags = G_APPLICATION_NON_UNIQUE;

  app = g_object_new (BOARDS_TYPE_APPLICATION,
                      "application-id", APP_ID,
                      "flags", flags,
                      "resource-base-path", "/io/sam/Boards",
                      NULL);

  app->standalone = !!standalone;

  return app;
}

/* boards-application.h
 *
 * Copyright 2022 Salim Monder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <adwaita.h>

#include "boards-version-macros.h"
#include "boards-window.h"

G_BEGIN_DECLS

#define BOARDS_TYPE_APPLICATION (boards_application_get_type())
#define BOARDS_APPLICATION_DEFAULT (BOARDS_APPLICATION (g_application_get_default()))

BOARDS_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (BoardsApplication, boards_application, BOARDS, APPLICATION, AdwApplication)


BOARDS_AVAILABLE_IN_ALL
BoardsWindow *      boards_application_get_current_window (BoardsApplication * self);

BOARDS_AVAILABLE_IN_ALL
GListModel *        boards_application_query_recent       (BoardsApplication * self,
                                                           GError **           error);

G_END_DECLS

/* boards-list.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "boards-list"

#include "config.h"
#include "boards-debug.h"

#include "boards-macros.h"
#include "boards-list.h"

struct _BoardsList
{
  GObject     parent_instance;

  gchar *     name;
  gchar *     title;
  gchar *     desc;

  GListStore *cards;

  guint       is_completed : 1;
  guint       modified : 1;
};

G_DEFINE_FINAL_TYPE (BoardsList, boards_list, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_NAME,
  PROP_TITLE,
  PROP_DESC,
  PROP_COMPLETED,
  PROP_MODIFIED,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
boards_list_set_name (BoardsList  *self,
                      const gchar *name)
{
  g_return_if_fail (BOARDS_IS_LIST (self));
  g_return_if_fail (name);

  if (!boards_str_equal (self->name, name))
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

static void
boards_list_finalize (GObject *object)
{
  BoardsList *self = (BoardsList *)object;

  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->title, g_free);
  g_clear_pointer (&self->desc, g_free);

  g_clear_object (&self->cards);

  G_OBJECT_CLASS (boards_list_parent_class)->finalize (object);
}

static void
boards_list_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  BoardsList *self = BOARDS_LIST (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, boards_list_get_name (self));
      break;

    case PROP_TITLE:
      g_value_set_string (value, boards_list_get_title (self));
      break;

    case PROP_DESC:
      g_value_set_string (value, boards_list_get_desc (self));
      break;

    case PROP_COMPLETED:
      g_value_set_boolean (value, boards_list_is_completed (self));
      break;

    case PROP_MODIFIED:
      g_value_set_boolean (value, boards_list_is_modified (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_list_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  BoardsList *self = BOARDS_LIST (object);

  switch (prop_id)
    {
    case PROP_NAME:
      boards_list_set_name (self, g_value_get_string (value));
      break;

    case PROP_TITLE:
      boards_list_set_title (self, g_value_get_string (value));
      break;

    case PROP_DESC:
      boards_list_set_desc (self, g_value_get_string (value));
      break;

    case PROP_COMPLETED:
      boards_list_set_completed (self, g_value_get_boolean (value));
      break;

    case PROP_MODIFIED:
      boards_list_set_modified (self, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_list_class_init (BoardsListClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = boards_list_finalize;
  object_class->get_property = boards_list_get_property;
  object_class->set_property = boards_list_set_property;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the list.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_TITLE] =
    g_param_spec_string ("title",
                         "Title",
                         "The title of the list.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_DESC] =
    g_param_spec_string ("desc",
                         "Description",
                         "A small description about the list.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_COMPLETED] =
    g_param_spec_boolean ("completed",
                          "Completed",
                          "If the list is completed.",
                          FALSE,
                          (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_MODIFIED] =
    g_param_spec_boolean ("modified",
                          "Modified",
                          "If the list is modified.",
                          FALSE,
                          (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
boards_list_init (BoardsList *self)
{
  self->cards = g_list_store_new (BOARDS_TYPE_CARD);
}


/**
 * boards_list_new:
 *
 * Creates a new #BoardsList.
 *
 * Returns: a newly created #BoardsList.
 *
 * Since: 0.1
 */
BoardsList *
boards_list_new (void)
{
  return g_object_new (BOARDS_TYPE_LIST, NULL);
}

gchar *
boards_list_get_name (BoardsList *self)
{
  g_return_val_if_fail (BOARDS_IS_LIST (self), NULL);

  return self->name;
}

gchar *
boards_list_get_title (BoardsList *self)
{
  g_return_val_if_fail (BOARDS_IS_LIST (self), NULL);

  return self->title;
}

void
boards_list_set_title (BoardsList  *self,
                       const gchar *title)
{
  g_return_if_fail (BOARDS_IS_LIST (self));
  g_return_if_fail (title);

  if (!boards_str_equal (self->title, title))
    {
      g_free (self->title);
      self->title = g_strdup (title);
      boards_list_set_name (self, boards_str_remove_spaces (title));
      boards_list_set_modified (self, TRUE);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TITLE]);
    }
}

gchar *
boards_list_get_desc (BoardsList *self)
{
  g_return_val_if_fail (BOARDS_IS_LIST (self), NULL);

  return self->desc;
}

void
boards_list_set_desc (BoardsList  *self,
                      const gchar *desc)
{
  g_return_if_fail (BOARDS_IS_LIST (self));
  g_return_if_fail (desc);

  if (!boards_str_equal (self->desc, desc))
    {
      g_free (self->desc);
      self->desc = g_strdup (desc);
      boards_list_set_modified (self, TRUE);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DESC]);
    }
}

gboolean
boards_list_is_completed (BoardsList *self)
{
  g_return_val_if_fail (BOARDS_IS_LIST (self), FALSE);

  return self->is_completed;
}

void
boards_list_set_completed (BoardsList *self,
                           gboolean    setting)
{
  g_return_if_fail (BOARDS_IS_LIST (self));

  setting = !! setting;

  if (self->is_completed != setting)
    {
      self->is_completed = setting;
      boards_list_set_modified (self, TRUE);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_COMPLETED]);
    }
}

gboolean
boards_list_is_modified (BoardsList *self)
{
  gboolean ret;

  g_return_val_if_fail (BOARDS_IS_LIST (self), FALSE);

  ret = self->modified;

  for (guint i = 0; i < boards_list_get_n_cards (self); i++)
    ret = ret || boards_card_is_modified (boards_list_get_card (self, i));

  return ret;
}

void
boards_list_set_modified (BoardsList *self,
                          gboolean    setting)
{
  g_return_if_fail (BOARDS_IS_LIST (self));

  setting = !! setting;

  if (self->modified != setting)
    {
      self->modified = setting;

      /* We need to propagate the state to all sub modules */
      for (guint i = 0; i < boards_list_get_n_cards (self); i++)
        boards_card_set_modified (boards_list_get_card (self, i), setting);

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_MODIFIED]);
    }
}


GListModel *
boards_list_get_cards (BoardsList *self)
{
  g_return_val_if_fail (BOARDS_IS_LIST (self), NULL);

  return (GListModel *)self->cards;
}

guint
boards_list_get_n_cards (BoardsList *self)
{
  g_return_val_if_fail (BOARDS_IS_LIST (self), 0);

  return g_list_model_get_n_items (G_LIST_MODEL (self->cards));
}

gboolean
boards_list_is_empty (BoardsList *self)
{
  g_return_val_if_fail (BOARDS_IS_LIST (self), FALSE);

  return (g_list_model_get_n_items (G_LIST_MODEL (self->cards)) == 0);
}

BoardsCard *
boards_list_get_card (BoardsList  *self,
                      const guint  card_id)
{
  g_return_val_if_fail (BOARDS_IS_LIST (self), NULL);

  return g_list_model_get_item (G_LIST_MODEL (self->cards), card_id);
}

void
boards_list_append_card (BoardsList *self,
                         BoardsCard *card)
{
  g_return_if_fail (BOARDS_IS_LIST (self));
  g_return_if_fail (BOARDS_IS_CARD (card));

  BOARDS_ENTRY;
  g_list_store_append (self->cards, card);
  boards_list_set_modified (self, TRUE);
  BOARDS_EXIT;
}

void
boards_list_insert_card (BoardsList  *self,
                         BoardsCard  *card,
                         const guint  position)
{
  g_return_if_fail (BOARDS_IS_LIST (self));
  g_return_if_fail (BOARDS_IS_CARD (card));

  BOARDS_ENTRY;

  if (position >= g_list_model_get_n_items (G_LIST_MODEL (self->cards)))
    {
      g_critical ("Error inserting card %s in list %s. Invalid position",
                  boards_card_get_title (card),
                  boards_list_get_title (self));
      BOARDS_EXIT;
    }

  g_list_store_insert (self->cards, position, card);
  boards_list_set_modified (self, TRUE);
  BOARDS_EXIT;
}

void
boards_list_remove_card (BoardsList  *self,
                         const guint  card_id)
{
  g_return_if_fail (BOARDS_IS_LIST (self));

  BOARDS_ENTRY;

  if (card_id >= g_list_model_get_n_items (G_LIST_MODEL (self->cards)))
    {
      g_critical ("Error deleting card from list %s. Invalid card id!",
                  boards_list_get_title(self));
      BOARDS_EXIT;
    }

  g_list_store_remove (self->cards, card_id);
  boards_list_set_modified (self, TRUE);
  BOARDS_EXIT;
}

void
boards_list_delete_card (BoardsList *self,
                         BoardsCard *card)
{
  guint position;

  g_return_if_fail (BOARDS_IS_LIST (self));
  g_return_if_fail (BOARDS_IS_CARD (card));

  BOARDS_ENTRY;

  if (g_list_store_find (self->cards, card, &position))
    {
      g_list_store_remove (self->cards, position);
      boards_list_set_modified (self, TRUE);
    }

  BOARDS_EXIT;
}



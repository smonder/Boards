/* boards-types.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include "boards-version-macros.h"

G_BEGIN_DECLS

#define BOARDS_TYPE_STATE	(boards_state_get_type())
#define BOARDS_IS_STATE(x) (((x) >= BOARDS_STATE_NORMAL || \
                             (x) < BOARDS_STATE_INVALID)? TRUE : FALSE)

/**
 * BoardsState:
 * @BOARDS_STATE_NONE: a normal state
 * @BOARDS_STATE_COMPLETED: card or task within is completed.
 * @BOARDS_STATE_POSTPONED: card or task within is postponed.
 * @BOARDS_STATE_WORKING: card or task within is currently working on.
 *
 * Describes a card or list state.
 *
 * Since: 0.1
 */
typedef enum
{
  BOARDS_STATE_NORMAL,
  BOARDS_STATE_COMPLETED,
  BOARDS_STATE_POSTPONED,
  BOARDS_STATE_WORKING,

  BOARDS_STATE_INVALID
} BoardsState;

BOARDS_AVAILABLE_IN_ALL
GType         boards_state_get_type (void) G_GNUC_CONST;
BOARDS_AVAILABLE_IN_1_0
const gchar * boards_state_str      (BoardsState   state);
BOARDS_AVAILABLE_IN_1_0
BoardsState   boards_state_from_str (const gchar * str);

G_END_DECLS

/* boards-card.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "boards-card"

#include "config.h"
#include "boards-debug.h"

#include "boards-macros.h"
#include "boards-card.h"

struct _BoardsCard
{
  GObject     parent_instance;

  gchar *     title;
  gchar *     desc;

  GDateTime * date_;
  BoardsState state;

  guint       is_completed : 1;
  guint       modified : 1;
};

G_DEFINE_FINAL_TYPE (BoardsCard, boards_card, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_TITLE,
  PROP_DESC,
  PROP_DATE,
  PROP_STATE,
  PROP_MODIFIED,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
boards_card_finalize (GObject *object)
{
  BoardsCard *self = (BoardsCard *)object;

  g_clear_pointer (&self->title, g_free);
  g_clear_pointer (&self->desc, g_free);

  g_clear_pointer (&self->date_, g_date_time_unref);

  G_OBJECT_CLASS (boards_card_parent_class)->finalize (object);
}

static void
boards_card_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  BoardsCard *self = BOARDS_CARD (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      g_value_set_string (value, boards_card_get_title (self));
      break;

    case PROP_DESC:
      g_value_set_string (value, boards_card_get_desc (self));
      break;

    case PROP_DATE:
      g_value_set_boxed (value, boards_card_get_date (self));
      break;

    case PROP_STATE:
      g_value_set_enum (value, boards_card_get_state (self));
      break;

    case PROP_MODIFIED:
      g_value_set_boolean (value, boards_card_is_modified (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_card_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  BoardsCard *self = BOARDS_CARD (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      boards_card_set_title (self, g_value_get_string (value));
      break;

    case PROP_DESC:
      boards_card_set_desc (self, g_value_get_string (value));
      break;

    case PROP_DATE:
      boards_card_set_date (self, g_value_get_boxed (value));
      break;

    case PROP_STATE:
      boards_card_set_state (self, g_value_get_enum (value));
      break;

    case PROP_MODIFIED:
      boards_card_set_modified (self, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_card_class_init (BoardsCardClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = boards_card_finalize;
  object_class->get_property = boards_card_get_property;
  object_class->set_property = boards_card_set_property;

  properties [PROP_TITLE] =
    g_param_spec_string ("title",
                         "Title",
                         "The title of the card.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_DESC] =
    g_param_spec_string ("desc",
                         "Description",
                         "A small description about the card.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_DATE] =
    g_param_spec_boxed ("date",
                        "Date",
                        "The date of the card.",
                        G_TYPE_DATE_TIME,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_STATE] =
    g_param_spec_enum ("state",
                       "State",
                       "The state of the card.",
                       BOARDS_TYPE_STATE, BOARDS_STATE_NORMAL,
                       (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_MODIFIED] =
    g_param_spec_boolean ("modified",
                          "Modified",
                          "If the card is modified.",
                          FALSE,
                          (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
boards_card_init (BoardsCard *self)
{
  self->date_ = g_date_time_new_now_local ();
}


/**
 * boards_card_new:
 *
 * Creates a new #BoardsCard.
 *
 * Returns: a newly created #BoardsCard.
 *
 * Since: 0.1
 */
BoardsCard *
boards_card_new (void)
{
  return g_object_new (BOARDS_TYPE_CARD, NULL);
}
gchar *
boards_card_get_title (BoardsCard *self)
{
  g_return_val_if_fail (BOARDS_IS_CARD (self), NULL);

  return self->title;
}

void
boards_card_set_title (BoardsCard  *self,
                       const gchar *title)
{
  g_return_if_fail (BOARDS_IS_CARD (self));
  g_return_if_fail (title);

  if (!boards_str_equal (self->title, title))
    {
      g_free (self->title);
      self->title = g_strdup (title);
      boards_card_set_modified (self, TRUE);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TITLE]);
    }
}

gchar *
boards_card_get_desc (BoardsCard *self)
{
  g_return_val_if_fail (BOARDS_IS_CARD (self), NULL);

  return self->desc;
}

void
boards_card_set_desc (BoardsCard  *self,
                      const gchar *desc)
{
  g_return_if_fail (BOARDS_IS_CARD (self));
  g_return_if_fail (desc);

  if (!boards_str_equal (self->desc, desc))
    {
      g_free (self->desc);
      self->desc = g_strdup (desc);
      boards_card_set_modified (self, TRUE);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DESC]);
    }
}

GDateTime *
boards_card_get_date (BoardsCard *self)
{
  g_return_val_if_fail (BOARDS_IS_CARD (self), NULL);

  return self->date_;
}

void
boards_card_set_date (BoardsCard *self,
                      GDateTime  *date_)
{
  g_return_if_fail (BOARDS_IS_CARD (self));
  g_return_if_fail (date_);

  if (g_date_time_equal (self->date_, date_))
    return;

  g_date_time_unref (self->date_);
  self->date_ = g_date_time_ref (date_);
  boards_card_set_modified (self, TRUE);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DATE]);
}

BoardsState
boards_card_get_state (BoardsCard *self)
{
  g_return_val_if_fail (BOARDS_IS_CARD (self), BOARDS_STATE_INVALID);

  return self->state;
}

void
boards_card_set_state (BoardsCard  *self,
                       BoardsState  state)
{
  g_return_if_fail (BOARDS_IS_CARD (self));
  g_return_if_fail (BOARDS_IS_STATE (state));

  if (self->state != state)
    {
      self->state = state;
      boards_card_set_modified (self, TRUE);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_STATE]);
    }
}

gboolean
boards_card_is_modified (BoardsCard *self)
{
  g_return_val_if_fail (BOARDS_IS_CARD (self), FALSE);

  return self->modified;
}

void
boards_card_set_modified (BoardsCard *self,
                          gboolean    setting)
{
  g_return_if_fail (BOARDS_IS_CARD (self));

  setting = !! setting;

  if (self->modified != setting)
    {
      self->modified = setting;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_MODIFIED]);
    }
}

/* boards-card.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

#include "boards-types.h"
#include "boards-version-macros.h"

G_BEGIN_DECLS

#define BOARDS_TYPE_CARD (boards_card_get_type())

BOARDS_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (BoardsCard, boards_card, BOARDS, CARD, GObject)

BOARDS_AVAILABLE_IN_ALL
BoardsCard *  boards_card_new           (void) G_GNUC_WARN_UNUSED_RESULT;

BOARDS_AVAILABLE_IN_ALL
gchar *       boards_card_get_title     (BoardsCard * self);
BOARDS_AVAILABLE_IN_ALL
void          boards_card_set_title     (BoardsCard * self,
                                         const gchar *title);

BOARDS_AVAILABLE_IN_ALL
gchar *       boards_card_get_desc      (BoardsCard * self);
BOARDS_AVAILABLE_IN_ALL
void          boards_card_set_desc      (BoardsCard * self,
                                         const gchar *desc);

BOARDS_AVAILABLE_IN_ALL
GDateTime *   boards_card_get_date      (BoardsCard * self);
BOARDS_AVAILABLE_IN_ALL
void          boards_card_set_date      (BoardsCard * self,
                                         GDateTime *  date_);

BOARDS_AVAILABLE_IN_ALL
BoardsState   boards_card_get_state     (BoardsCard * self);
BOARDS_AVAILABLE_IN_ALL
void          boards_card_set_state     (BoardsCard * self,
                                         BoardsState  state);

BOARDS_AVAILABLE_IN_ALL
gboolean     boards_card_is_modified    (BoardsCard *  self);
BOARDS_AVAILABLE_IN_ALL
void         boards_card_set_modified   (BoardsCard *  self,
                                         gboolean      setting);

G_END_DECLS

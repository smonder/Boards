/* boards-page.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

#include "boards-list.h"
#include "boards-card.h"

#include "boards-version-macros.h"

G_BEGIN_DECLS

#define BOARDS_PAGE_ERROR (boards_page_error_quark())
#define BOARDS_TYPE_PAGE (boards_page_get_type())

BOARDS_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (BoardsPage, boards_page, BOARDS, PAGE, GObject)

typedef enum {
  BOARDS_PAGE_INVALID_FILE_FORMAT = 1,
  BOARDS_PAGE_WRITE_ERROR,
} BoardsPageError;

BOARDS_AVAILABLE_IN_ALL
BoardsPage * boards_page_new            (void) G_GNUC_WARN_UNUSED_RESULT;
BOARDS_AVAILABLE_IN_ALL
GQuark       boards_page_error_quark    (void);

BOARDS_AVAILABLE_IN_ALL
gchar *      boards_page_get_name       (BoardsPage *  self);

BOARDS_AVAILABLE_IN_ALL
gchar *      boards_page_get_title      (BoardsPage *  self);
BOARDS_AVAILABLE_IN_ALL
void         boards_page_set_title      (BoardsPage *  self,
                                         const gchar * title);

BOARDS_AVAILABLE_IN_ALL
gchar *      boards_page_get_desc       (BoardsPage *  self);
BOARDS_AVAILABLE_IN_ALL
void         boards_page_set_desc       (BoardsPage *  self,
                                         const gchar * desc);

BOARDS_AVAILABLE_IN_ALL
GFile *      boards_page_get_file       (BoardsPage *  self);
BOARDS_AVAILABLE_IN_ALL
void         boards_page_set_file       (BoardsPage *  self,
                                         GFile *       file);

BOARDS_AVAILABLE_IN_ALL
GListModel * boards_page_get_lists      (BoardsPage *  self);
BOARDS_AVAILABLE_IN_ALL
guint        boards_page_get_n_lists    (BoardsPage *  self);
BOARDS_AVAILABLE_IN_ALL
void         boards_page_append_list    (BoardsPage *  self,
                                         BoardsList *  list);
BOARDS_AVAILABLE_IN_ALL
void         boards_page_insert_list    (BoardsPage *  self,
                                         BoardsList *  list,
                                         const guint   position);
BOARDS_AVAILABLE_IN_ALL
void         boards_page_insert_card    (BoardsPage *  self,
                                         BoardsCard *  card,
                                         const guint   list_id,
                                         const guint   position);
BOARDS_AVAILABLE_IN_ALL
BoardsList * boards_page_get_list       (BoardsPage *  self,
                                         const guint   list_id);
BOARDS_AVAILABLE_IN_ALL
BoardsCard * boards_page_get_card       (BoardsPage *  self,
                                         const guint   list_id,
                                         const guint   card_id);
BOARDS_AVAILABLE_IN_ALL
void         boards_page_remove_list    (BoardsPage *  self,
                                         const guint   list_id);
BOARDS_AVAILABLE_IN_ALL
void         boards_page_remove_card    (BoardsPage *  self,
                                         const guint   list_id,
                                         const guint   card_id);
BOARDS_AVAILABLE_IN_ALL
void         boards_page_delete_list    (BoardsPage *  self,
                                         BoardsList *  list);

BOARDS_AVAILABLE_IN_ALL
gboolean     boards_page_is_modified    (BoardsPage *  self);
BOARDS_AVAILABLE_IN_ALL
void         boards_page_set_modified   (BoardsPage *  self,
                                         gboolean      setting);

BOARDS_AVAILABLE_IN_ALL
gboolean     boards_page_save_to_file   (BoardsPage *  self,
                                         GFile *       file,
                                         GCancellable *cancellable,
                                         GError **     error);
BOARDS_AVAILABLE_IN_ALL
gboolean     boards_page_load_from_file (BoardsPage *  self,
                                         GFile *       file,
                                         GCancellable *cancellable,
                                         GError **     error);


G_END_DECLS

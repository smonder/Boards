/* boards-list.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

#include "boards-card.h"
#include "boards-version-macros.h"

G_BEGIN_DECLS

#define BOARDS_TYPE_LIST (boards_list_get_type())

BOARDS_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (BoardsList, boards_list, BOARDS, LIST, GObject)


BOARDS_AVAILABLE_IN_ALL
BoardsList *  boards_list_new           (void) G_GNUC_WARN_UNUSED_RESULT;

BOARDS_AVAILABLE_IN_ALL
gchar *       boards_list_get_name      (BoardsList * self);

BOARDS_AVAILABLE_IN_ALL
gchar *       boards_list_get_title     (BoardsList * self);
BOARDS_AVAILABLE_IN_ALL
void          boards_list_set_title     (BoardsList * self,
                                         const gchar *title);

BOARDS_AVAILABLE_IN_ALL
gchar *       boards_list_get_desc      (BoardsList * self);
BOARDS_AVAILABLE_IN_ALL
void          boards_list_set_desc      (BoardsList * self,
                                         const gchar *desc);

BOARDS_AVAILABLE_IN_ALL
gboolean      boards_list_is_completed  (BoardsList * self);
BOARDS_AVAILABLE_IN_ALL
void          boards_list_set_completed (BoardsList * self,
                                         gboolean     setting);

BOARDS_AVAILABLE_IN_ALL
gboolean     boards_list_is_modified    (BoardsList *  self);
BOARDS_AVAILABLE_IN_ALL
void         boards_list_set_modified   (BoardsList *  self,
                                         gboolean      setting);

BOARDS_AVAILABLE_IN_ALL
GListModel *  boards_list_get_cards     (BoardsList * self);
BOARDS_AVAILABLE_IN_ALL
guint         boards_list_get_n_cards   (BoardsList * self);
BOARDS_AVAILABLE_IN_ALL
gboolean      boards_list_is_empty      (BoardsList * self) G_GNUC_CONST;
BOARDS_AVAILABLE_IN_ALL
BoardsCard *  boards_list_get_card      (BoardsList * self,
                                         const guint  card_id);
BOARDS_AVAILABLE_IN_ALL
void          boards_list_append_card   (BoardsList * self,
                                         BoardsCard * card);
BOARDS_AVAILABLE_IN_ALL
void          boards_list_insert_card   (BoardsList * self,
                                         BoardsCard * card,
                                         const guint  position);
BOARDS_AVAILABLE_IN_ALL
void          boards_list_remove_card   (BoardsList * self,
                                         const guint  card_id);
BOARDS_AVAILABLE_IN_ALL
void          boards_list_delete_card   (BoardsList * self,
                                         BoardsCard * card);

G_END_DECLS

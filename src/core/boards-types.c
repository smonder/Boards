/* boards-types.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "boards-types"

#include "config.h"
#include "boards-types.h"

GType
boards_state_get_type (void)
{
	static GType boards_state = 0;

	if (boards_state == 0)
	{
		static const GEnumValue values[] = {
      { BOARDS_STATE_NORMAL,    "BOARDS_STATE_NORMAL",    "normal"    },
      { BOARDS_STATE_COMPLETED, "BOARDS_STATE_COMPLETED", "completed" },
      { BOARDS_STATE_POSTPONED, "BOARDS_STATE_POSTPONED", "postponed" },
      { BOARDS_STATE_WORKING,   "BOARDS_STATE_WORKING",   "working"   },
			{ 0, NULL, NULL }
		};
		boards_state = g_enum_register_static (
				g_intern_static_string ("BoardsState"),
				values);
	}
	return boards_state;
}

const gchar *
boards_state_str (BoardsState state)
{
  GEnumClass *enum_class;
  GEnumValue *enum_value;
  const gchar *ret;

  g_return_val_if_fail (BOARDS_IS_STATE (state), NULL);

  enum_class = g_type_class_ref (BOARDS_TYPE_STATE);
  enum_value = g_enum_get_value (enum_class, state);

  ret = g_strdup (enum_value->value_nick);
  g_type_class_unref (enum_class);

  return ret;
}

BoardsState
boards_state_from_str (const gchar *str)
{
  GEnumClass *enum_class;
  GEnumValue *enum_value;
  BoardsState ret;

  g_return_val_if_fail (str != NULL, BOARDS_STATE_INVALID);

  enum_class = g_type_class_ref (BOARDS_TYPE_STATE);
  enum_value = g_enum_get_value_by_nick (enum_class, str);

  ret = enum_value->value;
  g_type_class_unref (enum_class);

  return ret;
}

/* boards-version-macros.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib.h>

#include "boards-version.h"

#ifndef _BOARDS_EXTERN
#define _BOARDS_EXTERN extern
#endif

#ifdef BOARDS_DISABLE_DEPRECATION_WARNINGS
# define BOARDS_DEPRECATED _BOARDS_EXTERN
# define BOARDS_DEPRECATED_FOR(f) _BOARDS_EXTERN
# define BOARDS_UNAVAILABLE(maj,min) _BOARDS_EXTERN
#else
# define BOARDS_DEPRECATED G_DEPRECATED _BOARDS_EXTERN
# define BOARDS_DEPRECATED_FOR(f) G_DEPRECATED_FOR(f) _BOARDS_EXTERN
# define BOARDS_UNAVAILABLE(maj,min) G_UNAVAILABLE(maj,min) _BOARDS_EXTERN
#endif

#define BOARDS_VERSION_1_0 (G_ENCODE_VERSION (1, 0))

#if (BOARDS_MINOR_VERSION == 99)
# define BOARDS_VERSION_CUR_STABLE (G_ENCODE_VERSION (BOARDS_MAJOR_VERSION + 1, 0))
#elif (BOARDS_MINOR_VERSION % 2)
# define BOARDS_VERSION_CUR_STABLE (G_ENCODE_VERSION (BOARDS_MAJOR_VERSION, BOARDS_MINOR_VERSION + 1))
#else
# define BOARDS_VERSION_CUR_STABLE (G_ENCODE_VERSION (BOARDS_MAJOR_VERSION, BOARDS_MINOR_VERSION))
#endif

#if (BOARDS_MINOR_VERSION == 99)
# define BOARDS_VERSION_PREV_STABLE (G_ENCODE_VERSION (BOARDS_MAJOR_VERSION + 1, 0))
#elif (BOARDS_MINOR_VERSION % 2)
# define BOARDS_VERSION_PREV_STABLE (G_ENCODE_VERSION (BOARDS_MAJOR_VERSION, BOARDS_MINOR_VERSION - 1))
#else
# define BOARDS_VERSION_PREV_STABLE (G_ENCODE_VERSION (BOARDS_MAJOR_VERSION, BOARDS_MINOR_VERSION - 2))
#endif

/**
 * BOARDS_VERSION_MIN_REQUIRED:
 *
 * A macro that should be defined by the user prior to including
 * the drafting.h header.
 *
 * The definition should be one of the predefined Drafting version
 * macros: %BOARDS_VERSION_1_0, ...
 *
 * This macro defines the lower bound for the Drafting API to use.
 *
 * If a function has been deprecated in a newer version of Drafting,
 * it is possible to use this symbol to avoid the compiler warnings
 * without disabling warning for every deprecated function.
 */
#ifndef BOARDS_VERSION_MIN_REQUIRED
# define BOARDS_VERSION_MIN_REQUIRED (BOARDS_VERSION_CUR_STABLE)
#endif

/**
 * BOARDS_VERSION_MAX_ALLOWED:
 *
 * A macro that should be defined by the user prior to including
 * the drafting.h header.

 * The definition should be one of the predefined Drafting version
 * macros: %BOARDS_VERSION_1_0, %BOARDS_VERSION_1_2,...
 *
 * This macro defines the upper bound for the Drafting API to use.
 *
 * If a function has been introduced in a newer version of Drafting,
 * it is possible to use this symbol to get compiler warnings when
 * trying to use that function.
 */
#ifndef BOARDS_VERSION_MAX_ALLOWED
# if BOARDS_VERSION_MIN_REQUIRED > BOARDS_VERSION_PREV_STABLE
#  define BOARDS_VERSION_MAX_ALLOWED (BOARDS_VERSION_MIN_REQUIRED)
# else
#  define BOARDS_VERSION_MAX_ALLOWED (BOARDS_VERSION_CUR_STABLE)
# endif
#endif

#if BOARDS_VERSION_MAX_ALLOWED < BOARDS_VERSION_MIN_REQUIRED
#error "BOARDS_VERSION_MAX_ALLOWED must be >= BOARDS_VERSION_MIN_REQUIRED"
#endif
#if BOARDS_VERSION_MIN_REQUIRED < BOARDS_VERSION_1_0
#error "BOARDS_VERSION_MIN_REQUIRED must be >= BOARDS_VERSION_1_0"
#endif

#define BOARDS_AVAILABLE_IN_ALL                  _BOARDS_EXTERN

#if BOARDS_VERSION_MIN_REQUIRED >= BOARDS_VERSION_1_0
# define BOARDS_DEPRECATED_IN_1_0                BOARDS_DEPRECATED
# define BOARDS_DEPRECATED_IN_1_0_FOR(f)         BOARDS_DEPRECATED_FOR(f)
#else
# define BOARDS_DEPRECATED_IN_1_0                _BOARDS_EXTERN
# define BOARDS_DEPRECATED_IN_1_0_FOR(f)         _BOARDS_EXTERN
#endif

#if BOARDS_VERSION_MAX_ALLOWED < BOARDS_VERSION_1_0
# define BOARDS_AVAILABLE_IN_1_0                 BOARDS_UNAVAILABLE(1, 0)
#else
# define BOARDS_AVAILABLE_IN_1_0                 _BOARDS_EXTERN
#endif



/* boards-page.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "boards-page"

#include "config.h"
#include "boards-debug.h"

#include <glib/gi18n.h>
#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>

#include "boards-macros.h"
#include "boards-page.h"
#include "xml-reader-private.h"

#define XML_ENCODING "UTF-8"
#define XML_VERSION "1.0"

struct _BoardsPage
{
  GObject     parent_instance;

  gchar *     name;
  gchar *     title;
  gchar *     desc;

  GFile *     file;

  GListStore *lists;

  guint       modified : 1;
};

G_DEFINE_QUARK (boards_page_error, boards_page_error)
G_DEFINE_FINAL_TYPE (BoardsPage, boards_page, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_NAME,
  PROP_TITLE,
  PROP_DESC,
  PROP_FILE,
  PROP_MODIFIED,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


static void
boards_page_set_name (BoardsPage  *self,
                       const gchar *name)
{
  g_return_if_fail (BOARDS_IS_PAGE (self));
  g_return_if_fail (name);

  if (!boards_str_equal (self->name, name))
    {
      g_free (self->name);
      self->name = g_strdup (name);
      boards_page_set_modified (self, TRUE);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

static void
boards_page_finalize (GObject *object)
{
  BoardsPage *self = (BoardsPage *)object;

  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->title, g_free);
  g_clear_pointer (&self->desc, g_free);

  g_clear_object (&self->lists);

  G_OBJECT_CLASS (boards_page_parent_class)->finalize (object);
}

static void
boards_page_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  BoardsPage *self = BOARDS_PAGE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, boards_page_get_name (self));
      break;

    case PROP_TITLE:
      g_value_set_string (value, boards_page_get_title (self));
      break;

    case PROP_DESC:
      g_value_set_string (value, boards_page_get_desc (self));
      break;

    case PROP_FILE:
      g_value_set_object (value, boards_page_get_file (self));
      break;

    case PROP_MODIFIED:
      g_value_set_boolean (value, boards_page_is_modified (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_page_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  BoardsPage *self = BOARDS_PAGE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      boards_page_set_name (self, g_value_get_string (value));
      break;

    case PROP_TITLE:
      boards_page_set_title (self, g_value_get_string (value));
      break;

    case PROP_DESC:
      boards_page_set_desc (self, g_value_get_string (value));
      break;

    case PROP_FILE:
      boards_page_set_file (self, g_value_get_object (value));
      break;

    case PROP_MODIFIED:
      boards_page_set_modified (self, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_page_class_init (BoardsPageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = boards_page_finalize;
  object_class->get_property = boards_page_get_property;
  object_class->set_property = boards_page_set_property;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the page.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_TITLE] =
    g_param_spec_string ("title",
                         "Title",
                         "The title of the page.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_DESC] =
    g_param_spec_string ("desc",
                         "Description",
                         "A small description about the page.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_FILE] =
    g_param_spec_object ("file",
                         "File",
                         "The GFile to save the page to.",
                         G_TYPE_FILE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_MODIFIED] =
    g_param_spec_boolean ("modified",
                          "Modified",
                          "If the page is modified.",
                          FALSE,
                          (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
boards_page_init (BoardsPage *self)
{
  self->lists = g_list_store_new (BOARDS_TYPE_LIST);
}


/**
 * boards_page_new:
 *
 * Creates a new #BoardsPage.
 *
 * Returns: a newly created #BoardsPage.
 *
 * Since: 0.1
 */
BoardsPage *
boards_page_new (void)
{
  return g_object_new (BOARDS_TYPE_PAGE, NULL);
}

gchar *
boards_page_get_name (BoardsPage *self)
{
  g_return_val_if_fail (BOARDS_IS_PAGE (self), NULL);

  return self->name;
}

gchar *
boards_page_get_title (BoardsPage *self)
{
  g_return_val_if_fail (BOARDS_IS_PAGE (self), NULL);

  return self->title;
}

void
boards_page_set_title (BoardsPage  *self,
                       const gchar *title)
{
  g_return_if_fail (BOARDS_IS_PAGE (self));
  g_return_if_fail (title);

  if (!boards_str_equal (self->title, title))
    {
      g_free (self->title);
      self->title = g_strdup (title);
      boards_page_set_name (self, boards_str_remove_spaces (title));
      boards_page_set_modified (self, TRUE);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TITLE]);
    }
}

gchar *
boards_page_get_desc (BoardsPage *self)
{
  g_return_val_if_fail (BOARDS_IS_PAGE (self), NULL);

  return self->desc;
}

void
boards_page_set_desc (BoardsPage  *self,
                      const gchar *desc)
{
  g_return_if_fail (BOARDS_IS_PAGE (self));
  g_return_if_fail (desc);

  if (!boards_str_equal (self->desc, desc))
    {
      g_free (self->desc);
      self->desc = g_strdup (desc);
      boards_page_set_modified (self, TRUE);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DESC]);
    }
}

gboolean
boards_page_is_modified (BoardsPage *self)
{
  gboolean ret;

  g_return_val_if_fail (BOARDS_IS_PAGE (self), FALSE);

  ret = self->modified;

  for (guint i = 0; i < boards_page_get_n_lists (self); i++)
    ret = ret || boards_list_is_modified (boards_page_get_list (self, i));

  return ret;
}

void
boards_page_set_modified (BoardsPage *self,
                          gboolean    setting)
{
  g_return_if_fail (BOARDS_IS_PAGE (self));

  setting = !! setting;

  if (self->modified != setting)
    {
      self->modified = setting;

      /* We need to propagate the state to all sub modules */
      for (guint i = 0; i < boards_page_get_n_lists (self); i++)
        boards_list_set_modified (boards_page_get_list (self, i), setting);

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_MODIFIED]);
    }
}

GFile *
boards_page_get_file (BoardsPage *self)
{
  g_return_val_if_fail (BOARDS_IS_PAGE (self), NULL);

  return self->file;
}

void
boards_page_set_file (BoardsPage *self,
                      GFile      *file)
{
  g_return_if_fail (BOARDS_IS_PAGE (self));
  g_return_if_fail (!file || G_IS_FILE (file));

  BOARDS_ENTRY;
  if (file == NULL)
    {
      g_clear_object (&self->file);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_FILE]);
      BOARDS_EXIT;
    }

  if (self->file == file)
    BOARDS_EXIT;

  if (g_set_object (&self->file, file))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_FILE]);

  BOARDS_EXIT;
}

GListModel *
boards_page_get_lists (BoardsPage *self)
{
  g_return_val_if_fail (BOARDS_IS_PAGE (self), NULL);

  return (GListModel *)self->lists;
}

guint
boards_page_get_n_lists (BoardsPage *self)
{
  g_return_val_if_fail (BOARDS_IS_PAGE (self), 0);

  return g_list_model_get_n_items (G_LIST_MODEL (self->lists));
}

void
boards_page_append_list (BoardsPage *self,
                         BoardsList *list)
{
  g_return_if_fail (BOARDS_IS_PAGE (self));
  g_return_if_fail (BOARDS_IS_LIST (list));

  BOARDS_ENTRY;
  g_list_store_append (self->lists, list);
  boards_page_set_modified (self, TRUE);
  BOARDS_EXIT;
}

void
boards_page_insert_list (BoardsPage  *self,
                         BoardsList  *list,
                         const guint  position)
{
  g_return_if_fail (BOARDS_IS_PAGE (self));
  g_return_if_fail (BOARDS_IS_LIST (list));

  BOARDS_ENTRY;

  if (position >= boards_page_get_n_lists (self))
    {
      g_critical ("Error adding list %s to page %s. Invalid position!",
                  boards_list_get_name (list),
                  boards_page_get_name (self));
      BOARDS_EXIT;
    }

  g_list_store_insert (self->lists, position, list);
  boards_page_set_modified (self, TRUE);

  BOARDS_EXIT;
}

void
boards_page_insert_card (BoardsPage  *self,
                         BoardsCard  *card,
                         const guint  list_id,
                         const guint  position)
{
  g_return_if_fail (BOARDS_IS_PAGE (self));
  g_return_if_fail (BOARDS_IS_CARD (card));

  BOARDS_ENTRY;

  if (list_id >= boards_page_get_n_lists (self))
    {
      g_critical ("Error adding card %s to page %s. Invalid list id!",
                  boards_card_get_title (card),
                  boards_page_get_title (self));
      BOARDS_EXIT;
    }

  boards_list_insert_card (boards_page_get_list (self, list_id), card, position);
  boards_page_set_modified (self, TRUE);

  BOARDS_EXIT;
}

BoardsList *
boards_page_get_list (BoardsPage  *self,
                      const guint  list_id)
{
  g_return_val_if_fail (BOARDS_IS_PAGE (self), NULL);

  BOARDS_ENTRY;

  if (list_id >= boards_page_get_n_lists (self))
    {
      g_critical ("Error getting list from page %s. Invalid list id!",
                  boards_page_get_name (self));
      BOARDS_RETURN (NULL);
    }

  BOARDS_RETURN (g_list_model_get_item (G_LIST_MODEL (self->lists), list_id));
}

BoardsCard *
boards_page_get_card (BoardsPage  *self,
                      const guint  list_id,
                      const guint  card_id)
{
  BoardsList *list;
  g_return_val_if_fail (BOARDS_IS_PAGE (self), NULL);

  BOARDS_ENTRY;

  if (list_id >= boards_page_get_n_lists (self))
    {
      g_critical ("Error getting card from page %s. Invalid list id!",
                  boards_page_get_name (self));
      BOARDS_RETURN (NULL);
    }

  list = g_list_model_get_item (G_LIST_MODEL (self->lists), list_id);
  BOARDS_RETURN (boards_list_get_card (list, card_id));
}

void
boards_page_remove_list (BoardsPage *self,
                         const guint  list_id)
{
  g_return_if_fail (BOARDS_IS_PAGE (self));

  BOARDS_ENTRY;

  if (list_id >= boards_page_get_n_lists (self))
    {
      g_critical ("Error deleting list from page %s. Invalid list id!",
                  boards_page_get_name (self));
      BOARDS_EXIT;
    }

  g_list_store_remove (self->lists, list_id);
  boards_page_set_modified (self, TRUE);

  BOARDS_EXIT;
}

void
boards_page_delete_list (BoardsPage *self,
                         BoardsList *list)
{
  guint position;

  g_return_if_fail (BOARDS_IS_PAGE (self));
  g_return_if_fail (BOARDS_IS_LIST (list));

  BOARDS_ENTRY;

  if (g_list_store_find (self->lists, list, &position))
    {
      boards_page_remove_list (self, position);
      boards_page_set_modified (self, TRUE);
      BOARDS_EXIT;
    }

  g_critical ("Error deleting list from page %s. List is not in page.",
              boards_page_get_name (self));
  BOARDS_EXIT;
}

void
boards_page_remove_card (BoardsPage  *self,
                         const guint  list_id,
                         const guint  card_id)
{
  BoardsList *list;
  g_return_if_fail (BOARDS_IS_PAGE (self));

  BOARDS_ENTRY;

  if (list_id >= boards_page_get_n_lists (self))
    {
      g_critical ("Error deleting list from page %s. Invalid list id!",
                  boards_page_get_name (self));
      BOARDS_EXIT;
    }

  list = g_list_model_get_item (G_LIST_MODEL (self->lists), list_id);
  boards_list_remove_card (list, card_id);
  boards_page_set_modified (self, TRUE);
  BOARDS_EXIT;
}


static void
write_list (BoardsList *list,
            xmlNodePtr  page_node)
{
  GListModel *cards = NULL;
  xmlNodePtr list_node;
  xmlNodePtr card_node;

  g_assert (BOARDS_IS_LIST (list));

  cards = boards_list_get_cards (list);

  list_node = xmlNewChild (page_node, NULL, BAD_CAST "list", NULL);
  xmlNewProp (list_node, BAD_CAST "name", BAD_CAST boards_list_get_name (list));
  xmlNewProp (list_node, BAD_CAST "title", BAD_CAST boards_list_get_title (list));
  xmlNewProp (list_node, BAD_CAST "desc", BAD_CAST boards_list_get_desc (list));

  for (guint i = 0; i < g_list_model_get_n_items (cards); i++)
    {
      BoardsCard *card = boards_list_get_card (list, i);
      const gchar *date_format;

      date_format = g_date_time_format (boards_card_get_date (card), "%e/%m/%Y");

      card_node = xmlNewChild (list_node, NULL, BAD_CAST "card", BAD_CAST boards_card_get_desc (card));
      xmlNewProp (card_node, BAD_CAST "title", BAD_CAST boards_card_get_title (card));
      xmlNewProp (card_node, BAD_CAST "state", BAD_CAST boards_state_str (boards_card_get_state (card)));
      xmlNewProp (card_node, BAD_CAST "date", BAD_CAST date_format);
    }
}

/**
 * boards_page_save_to_file:
 * @self: a #BoardsPage.
 * @file: a #GFile to save to.
 * @cancellable: a #GCancellable.
 * @error: a #GError.
 *
 * Save the data of @self into @file.
 * if this operation is failed, %FALSE is returned and @error is set.
 *
 * Returns: (transfer full): %TRUE in success. %FALSE otherwise.
 *
 * Since: 0.1
 */
gboolean
boards_page_save_to_file (BoardsPage    *self,
                          GFile         *file,
                          GCancellable  *cancellable,
                          GError       **error)
{
  xmlDocPtr doc;
  xmlNodePtr page_node;

  g_return_val_if_fail (BOARDS_IS_PAGE (self), FALSE);
  g_return_val_if_fail (G_IS_FILE (file), FALSE);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  BOARDS_ENTRY;
#if defined(LIBXML_WRITER_ENABLED) && defined(LIBXML_OUTPUT_ENABLED)
  doc = xmlNewDoc (BAD_CAST XML_DEFAULT_VERSION);
  page_node = xmlNewNode (NULL, BAD_CAST "page");
  xmlDocSetRootElement (doc, page_node);

  /* xmlCreateIntSubset (doc, BAD_CAST "page", NULL, BAD_CAST"page.dtd"); */

  xmlNewChild (page_node, NULL, BAD_CAST "name", BAD_CAST boards_page_get_name (self));
  xmlNewChild (page_node, NULL, BAD_CAST "title", BAD_CAST boards_page_get_title (self));
  xmlNewChild (page_node, NULL, BAD_CAST "desc", BAD_CAST boards_page_get_desc (self));

  for (guint i = 0; i < boards_page_get_n_lists (self); i++)
    {
      BoardsList * list = boards_page_get_list (self, i);
      write_list (list, page_node);
    }

  xmlSaveFormatFileEnc (g_file_get_uri (file), doc, XML_ENCODING, 1);
  xmlFreeDoc (doc);
  xmlCleanupParser ();
  xmlMemoryDump ();

  boards_page_set_file (self, file);
  boards_page_set_modified (self, FALSE);
  BOARDS_RETURN (TRUE);
#else
  g_set_error (error, BOARDS_PAGE_ERROR, BOARDS_PAGE_WRITE_ERROR,
               "Writer or output support not compiled in");
  BOARDS_RETURN (FALSE);
#endif
}

static gboolean
parse_list (BoardsPage *self,
            BoardsList *list,
            XmlReader  *reader)
{
  g_assert (BOARDS_IS_PAGE (self));
  g_assert (BOARDS_IS_LIST (list));
  g_assert (XML_IS_READER (reader));

  BOARDS_ENTRY;

  if (!xml_reader_read (reader))
    BOARDS_RETURN (FALSE);

  do
    {
      if (xml_reader_is_a_local (reader, "card"))
        {
          g_autoptr (BoardsCard) card = boards_card_new ();
          gchar *str;

          str = xml_reader_get_attribute (reader, "title");
          if (str != NULL)
            boards_card_set_title (card, g_strstrip (str));

          str = xml_reader_get_attribute (reader, "state");
          if (str != NULL)
            boards_card_set_state (card, boards_state_from_str (g_strstrip (str)));

          str = xml_reader_get_attribute (reader, "date");
          if (str != NULL)
            {
              GDateTime *dt;
              gint day, month, year;

              if (sscanf (g_strstrip (str), "%d/%d/%d", &day, &month, &year))
                {
                  dt = g_date_time_new (g_time_zone_new_local (),
                                        year, month, day,
                                        0, 0, 0);
                  boards_card_set_date (card, dt);
                }
            }

          str = xml_reader_read_string (reader);
          if (str != NULL)
            boards_card_set_desc (card, g_strstrip (str));


          if (boards_card_get_title (card))
            boards_list_append_card (list, card);
        }
    }
  while (xml_reader_is_a_local (reader, "card") && xml_reader_read_to_next (reader));

  BOARDS_RETURN (TRUE);
}

static gboolean
load_page (BoardsPage  *self,
           XmlReader   *reader,
           GError     **error)
{
  BOARDS_ENTRY;
  if (!xml_reader_read_start_element (reader, "page"))
    {
      g_set_error (error,
                   BOARDS_PAGE_ERROR,
                   BOARDS_PAGE_INVALID_FILE_FORMAT,
                   "Page element is missing from the file.");
      BOARDS_RETURN (FALSE);
    }

  g_object_freeze_notify (G_OBJECT (self));

  xml_reader_read (reader);
  do
    {
      const gchar *element_name;
      element_name = xml_reader_get_local_name (reader);

      if (boards_str_equal (element_name, "name") ||
          boards_str_equal (element_name, "title") ||
          boards_str_equal (element_name, "desc"))
        {
          gchar *str;

          str = xml_reader_read_string (reader);
          if (str != NULL)
            g_object_set (self, element_name, g_strstrip (str), NULL);
        }
      else if (boards_str_equal (element_name, "list"))
        {
          g_autoptr (BoardsList) list = NULL;
          gchar *str;

          list = boards_list_new ();

          str = xml_reader_get_attribute (reader, "title");
          if (str != NULL)
            boards_list_set_title (list, g_strstrip (str));

          str = xml_reader_get_attribute (reader, "desc");
          if (str != NULL)
            boards_list_set_desc (list, g_strstrip (str));

          str = xml_reader_get_attribute (reader, "completed");
          if (str != NULL)
            boards_list_set_completed (list, boards_str_equal (g_strstrip (str), "true"));

          if (!parse_list (self, list, reader))
            break;

          if (boards_list_get_name (list) || boards_list_get_title (list))
            boards_page_append_list (self, list);

          g_free (str);
        }
    }
  while (xml_reader_read_to_next (reader));

  g_object_thaw_notify (G_OBJECT (self));
  BOARDS_RETURN (TRUE);
}

/**
 * boards_page_load_from_file:
 * @self: a #BoardsPage.
 * @file: a #GFile to read from.
 * @cancellable: a #GCancellable.
 * @error: a #GError.
 *
 * Load the data of @self from @file.
 * if this operation is failed, %FALSE is returned and @error is set.
 *
 * Returns: (transfer full): %TRUE in success. %FALSE otherwise.
 *
 * Since: 0.1
 */
gboolean
boards_page_load_from_file (BoardsPage    *self,
                            GFile         *file,
                            GCancellable  *cancellable,
                            GError       **error)
{
  g_autoptr (XmlReader) reader = NULL;

  g_return_val_if_fail (BOARDS_IS_PAGE (self), FALSE);
  g_return_val_if_fail (G_IS_FILE (file), FALSE);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  BOARDS_ENTRY;

  reader = xml_reader_new ();

  if (!xml_reader_load_from_file (reader, file, cancellable, error))
    BOARDS_RETURN (FALSE);

  if (load_page (self, reader, error))
    {
      boards_page_set_file (self, file);
      boards_page_set_modified (self, FALSE);
      BOARDS_RETURN (TRUE);
    }

  BOARDS_RETURN (FALSE);
}


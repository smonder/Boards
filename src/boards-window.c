/* boards-window.c
 *
 * Copyright 2022 Salim Monder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "boards-window"

#include "config.h"
#include "boards-debug.h"

#include "boards-macros.h"
#include "boards-window-private.h"

G_DEFINE_TYPE (BoardsWindow, boards_window, ADW_TYPE_APPLICATION_WINDOW)

static void
on_surface_notify_visible_cb (BoardsWindow *self,
                              GParamSpec   *pspec,
                              GtkStack     *surfaces)
{
  GError *error = NULL;

  g_assert (BOARDS_IS_WINDOW (self));
  g_assert (GTK_IS_STACK (surfaces));
  g_assert (G_IS_PARAM_SPEC (pspec));

  if (BOARDS_IS_GREETER (gtk_stack_get_visible_child (surfaces)))
    {
      boards_greeter_bind_recents_list (BOARDS_GREETER (self->greeter),
                                        boards_application_query_recent (BOARDS_APPLICATION_DEFAULT, &error));
      if (error != NULL)
        g_critical ("Error retrieving recent boards! %s", error->message);
    }
}

static void
load_geometry (BoardsWindow *self)
{
  GSettings *settings;
  GtkWindow *window;
  gboolean maximized;
  gint height;
  gint width;

  g_assert (BOARDS_IS_WINDOW (self));

  window = GTK_WINDOW (self);
  settings = g_settings_new ("io.sam.Boards");

  maximized = g_settings_get_boolean (settings, "window-maximized");
  g_settings_get (settings, "window-size", "(ii)", &width, &height);

  gtk_window_set_default_size (window, width, height);

  if (maximized)
    gtk_window_maximize (window);
}

static void
boards_window_unmap (GtkWidget *widget)
{
  BoardsWindow *self = (BoardsWindow *)widget;
  GSettings *settings;
  gboolean maximized;

  settings = g_settings_new ("io.sam.Boards");
  maximized = gtk_window_is_maximized (GTK_WINDOW (self));
  g_settings_set_boolean (settings, "window-maximized", maximized);

  if (!maximized)
    {
      gint height;
      gint width;

      gtk_window_get_default_size (GTK_WINDOW (self), &width, &height);
      g_settings_set (settings, "window-size", "(ii)", width, height);
    }

  GTK_WIDGET_CLASS (boards_window_parent_class)->unmap (widget);
}

static void
boards_window_constructed (GObject *object)
{
  BoardsWindow *self = (BoardsWindow *)object;

  G_OBJECT_CLASS (boards_window_parent_class)->constructed (object);

  load_geometry (self);
}

static void
boards_window_class_init (BoardsWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = boards_window_constructed;
  widget_class->unmap = boards_window_unmap;

  g_type_ensure (BOARDS_TYPE_OMNIBAR);

  gtk_widget_class_set_template_from_resource (widget_class, "/ui/boards-window.ui");
  gtk_widget_class_bind_template_child (widget_class, BoardsWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, BoardsWindow, menu_button);
  gtk_widget_class_bind_template_child (widget_class, BoardsWindow, surfaces);
  gtk_widget_class_bind_template_child (widget_class, BoardsWindow, omnibar);
  gtk_widget_class_bind_template_child (widget_class, BoardsWindow, close_page);
}

static void
boards_window_init (BoardsWindow *self)
{
  GError *error = NULL;

  gtk_widget_init_template (GTK_WIDGET (self));

  _boards_window_init_actions (self);

  self->greeter = boards_greeter_new ("Greeter");
  boards_greeter_bind_recents_list (BOARDS_GREETER (self->greeter),
                                    boards_application_query_recent (BOARDS_APPLICATION_DEFAULT, &error));
  if (error != NULL)
    g_critical ("Error retrieving recent boards! %s", error->message);

  boards_window_add_surface (self, self->greeter);
  g_object_bind_property (self, "title", self->omnibar, "title", G_BINDING_SYNC_CREATE);
  g_signal_connect_swapped (self->surfaces, "notify::visible-child",
                            G_CALLBACK (on_surface_notify_visible_cb), self);
}


/**
 * boards_window_new:
 *
 * Creates a new #BoardsWindow.
 *
 * Returns: a #BoardsWindow.
 *
 * Since: 0.1
 */
BoardsWindow *
boards_window_new (void)
{
  return g_object_new (BOARDS_TYPE_WINDOW,
                       "application", BOARDS_APPLICATION_DEFAULT,
                       "title", PACKAGE_NAME,
                       "icon-name", APP_ID,
                       NULL);
}

/**
 * boards_window_add_surface:
 * @self: a #BoardsWindow.
 * @surface: a #BoardsSurface.
 *
 * Adds @surface to the surfaces list of @self.
 *
 * Since: 0.1
 */
void
boards_window_add_surface (BoardsWindow  *self,
                           BoardsSurface *surface)
{
  g_return_if_fail (BOARDS_IS_WINDOW (self));
  g_return_if_fail (BOARDS_IS_SURFACE (surface));

  BOARDS_ENTRY;
  gtk_stack_add_titled (self->surfaces, GTK_WIDGET (surface),
                        boards_surface_get_name (surface),
                        boards_surface_get_title (surface));
  gtk_stack_page_set_icon_name (gtk_stack_get_page (self->surfaces, GTK_WIDGET (surface)),
                                boards_surface_get_icon_name (surface));
  gtk_stack_set_visible_child (self->surfaces, GTK_WIDGET (surface));

  if (!BOARDS_IS_GREETER (surface))
    {
      g_object_bind_property (surface, "title",
                              self->omnibar, "title",
                              G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

      gtk_widget_set_visible (GTK_WIDGET (self->close_page), TRUE);

      if (BOARDS_IS_PAGE_VIEW (surface))
        boards_omnibar_set_page (self->omnibar,
                                  boards_page_view_get_page (BOARDS_PAGE_VIEW (surface)));
    }

  boards_surface_set (surface);
  g_set_weak_pointer (&self->current_surface, surface);

  BOARDS_EXIT;
}

/**
 * boards_window_get_surface:
 * @self: a #BoardsWindow.
 * @name: a #gchar: the name of surface to seek.
 *
 * Gets the #BoardsSurface that has a name @name.
 *
 * Returns: (transfer full) (nullable): a #BoardsSurface.
 *
 * Since: 0.1
 */
BoardsSurface *
boards_window_get_surface (BoardsWindow *self,
                           const gchar  *surface_name)
{
  GtkWidget *ret = NULL;

  g_return_val_if_fail (BOARDS_IS_WINDOW (self), NULL);
  g_return_val_if_fail (surface_name, NULL);

  BOARDS_ENTRY;
  ret = gtk_stack_get_child_by_name (self->surfaces, surface_name);

  if (BOARDS_IS_SURFACE (ret))
    BOARDS_RETURN (BOARDS_SURFACE (ret));

  g_critical ("No surface with the name %s!", surface_name);
  BOARDS_RETURN (NULL);
}

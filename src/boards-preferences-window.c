/* boards-preferences-window.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */
#define G_LOG_DOMAIN "boards-preferences-window"

#include "config.h"
#include "boards-debug.h"

#include <glib/gi18n.h>

#include "boards-application.h"
#include "boards-preferences-window.h"

struct _BoardsPreferencesWindow
{
  AdwPreferencesWindow parent_instance;

  GSettings * settings;

  /* -----< TEMPLATE WIDGETS >----- */
  AdwButtonContent * select_path_button;
};

G_DEFINE_FINAL_TYPE (BoardsPreferencesWindow, boards_preferences_window, ADW_TYPE_PREFERENCES_WINDOW)

enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


static void
on_select_location_dlg_response_cb (BoardsPreferencesWindow *self,
                                    gint                     response,
                                    GtkNativeDialog         *dlg)
{
  g_assert (BOARDS_IS_PREFERENCES_WINDOW (self));
  g_assert (GTK_IS_NATIVE_DIALOG (dlg));

  if (response == GTK_RESPONSE_ACCEPT)
    {
      GtkFileChooser *chooser = GTK_FILE_CHOOSER (dlg);
      g_autoptr(GFile) file = gtk_file_chooser_get_file (chooser);

      g_settings_set (self->settings, "boards-location", "s", g_file_get_path (file));
      adw_button_content_set_label (self->select_path_button,
                                    g_file_get_basename (file));
      gtk_widget_set_tooltip_text (GTK_WIDGET (self->select_path_button),
                                   g_file_get_path (file));
    }

  gtk_window_destroy (GTK_WINDOW (dlg));
}

static void
on_select_path_button_clicked_cb (BoardsPreferencesWindow *self,
                                  GtkButton               *button)
{
  GtkFileChooserNative *dlg;

  g_assert (BOARDS_IS_PREFERENCES_WINDOW (self));
  g_assert (GTK_IS_BUTTON (button));

  dlg = gtk_file_chooser_native_new (_("Select Location"), GTK_WINDOW (self),
                                     GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                     _("Select"), _("Cancel"));
  gtk_file_chooser_set_create_folders (GTK_FILE_CHOOSER (dlg), TRUE);
  gtk_file_chooser_set_select_multiple (GTK_FILE_CHOOSER (dlg), FALSE);

  g_signal_connect_swapped (dlg, "response", G_CALLBACK (on_select_location_dlg_response_cb), self);
  gtk_native_dialog_show (GTK_NATIVE_DIALOG (dlg));
}

static void
boards_preferences_window_finalize (GObject *object)
{
  BoardsPreferencesWindow *self = (BoardsPreferencesWindow *)object;

  G_OBJECT_CLASS (boards_preferences_window_parent_class)->finalize (object);
}

static void
boards_preferences_window_get_property (GObject    *object,
                                        guint       prop_id,
                                        GValue     *value,
                                        GParamSpec *pspec)
{
  BoardsPreferencesWindow *self = BOARDS_PREFERENCES_WINDOW (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_preferences_window_set_property (GObject      *object,
                                        guint         prop_id,
                                        const GValue *value,
                                        GParamSpec   *pspec)
{
  BoardsPreferencesWindow *self = BOARDS_PREFERENCES_WINDOW (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_preferences_window_class_init (BoardsPreferencesWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = boards_preferences_window_finalize;
  object_class->get_property = boards_preferences_window_get_property;
  object_class->set_property = boards_preferences_window_set_property;

  gtk_widget_class_set_template_from_resource (widget_class, "/ui/boards-preferences-window.ui");
  gtk_widget_class_bind_template_child (widget_class, BoardsPreferencesWindow, select_path_button);
  gtk_widget_class_bind_template_callback (widget_class, on_select_path_button_clicked_cb);
}

static void
boards_preferences_window_init (BoardsPreferencesWindow *self)
{
  const gchar *boards_location;
  gtk_widget_init_template (GTK_WIDGET (self));

  self->settings = g_settings_new ("io.sam.Boards");
  boards_location = g_settings_get_string (self->settings, "boards-location");
  adw_button_content_set_label (self->select_path_button,
                                g_file_get_basename (g_file_new_for_path (boards_location)));
  gtk_widget_set_tooltip_text (GTK_WIDGET (self->select_path_button),
                               boards_location);
}


/**
 * boards_preferences_window_new:
 *
 * Creates a new #BoardsPreferencesWindow.
 *
 * Returns: a newly created #GtkWindow
 *
 * Since: 0.1
 */
GtkWindow *
boards_preferences_window_new (void)
{
  return g_object_new (BOARDS_TYPE_PREFERENCES_WINDOW, NULL);
}

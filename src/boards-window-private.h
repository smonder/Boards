/* boards-window-private.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <libpanel.h>

#include "boards-application.h"
#include "boards-greeter.h"
#include "boards-omnibar.h"
#include "boards-page-view.h"
#include "boards-window.h"


struct _BoardsWindow
{
  AdwApplicationWindow  parent_instance;

  /* -----< TEMPLATE WIDGETS >----- */
  AdwHeaderBar *        header_bar;
  GtkMenuButton *       menu_button;
  GtkStack *            surfaces;
  BoardsOmnibar *       omnibar;
  GtkButton *           close_page;

  /* A Greeter surface that will be shown by default */
  BoardsSurface *       greeter;

  /* A weak pointer to current visible surface */
  BoardsSurface *       current_surface;
};



void           _boards_window_init_actions    (BoardsWindow * self);


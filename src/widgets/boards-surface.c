/* boards-surface.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "boards-surface"

#include "config.h"
#include "boards-debug.h"

#include "boards-macros.h"
#include "boards-surface.h"

typedef struct
{
  gchar * name;
  gchar * title;
  gchar * icon_name;

  /* The contents of the surface are going to be placed all
   * in a scrollable viewport area.
   */
  GtkScrolledWindow * viewport;

  /* And we need a menu that will show up if the user right-clicked
   * on the surface.
   */
  GtkPopoverMenu * right_click_menu;

  /* An event controller to listen to the right click event. */
  GtkGestureClick * click_gesture;
} BoardsSurfacePrivate;


static void buildable_iface_init (GtkBuildableIface *iface);

G_DEFINE_ABSTRACT_TYPE_WITH_CODE (BoardsSurface, boards_surface, GTK_TYPE_WIDGET,
                                  G_ADD_PRIVATE (BoardsSurface)
                                  G_IMPLEMENT_INTERFACE (GTK_TYPE_BUILDABLE, buildable_iface_init))

enum {
  PROP_0,
  PROP_NAME,
  PROP_TITLE,
  PROP_ICON_NAME,
  PROP_CHILD,
  PROP_MENU,
  N_PROPS
};

static GtkBuildableIface *parent_buildable_iface;
static GParamSpec *properties [N_PROPS];


/**
 * boards_surface_set_name:
 * @self: a #BoardsSurface.
 * @name: a #gchar.
 *
 * Sets the name property of @self.
 *
 * Since: 0.1
 */
static void
boards_surface_set_name (BoardsSurface * self,
                         const gchar *   name)
{
  BoardsSurfacePrivate *priv = boards_surface_get_instance_private (self);

  g_return_if_fail (BOARDS_IS_SURFACE (self));
  g_return_if_fail (name);

  BOARDS_ENTRY;

  if (!boards_str_equal (priv->name, name))
    {
      g_free (priv->name);
      priv->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }

  BOARDS_EXIT;
}

static void
on_gesture_click_pressed_cb (BoardsSurface   *self,
                             gint             n_press,
                             gdouble          x,
                             gdouble          y,
                             GtkGestureClick *gesture)
{
  BoardsSurfacePrivate *priv = boards_surface_get_instance_private (self);

  g_assert (BOARDS_IS_SURFACE (self));
  g_assert (GTK_IS_GESTURE_CLICK (gesture));

  if (gtk_popover_menu_get_menu_model (priv->right_click_menu))
    {
      const GdkRectangle rect = { x, y, 0, 0 };

      gtk_popover_set_pointing_to (GTK_POPOVER (priv->right_click_menu), &rect);
      gtk_popover_set_offset (GTK_POPOVER (priv->right_click_menu), - 20, 0);
      gtk_popover_popup (GTK_POPOVER (priv->right_click_menu));
    }
}


static void
boards_surface_add_child (GtkBuildable *buildable,
                          GtkBuilder   *builder,
                          GObject      *child,
                          const gchar  *type)
{
  BoardsSurface *self = (BoardsSurface *)buildable;

  g_assert (BOARDS_IS_SURFACE (self));

  if (!child || !GTK_IS_WIDGET (child))
    g_error ("BoardsSurface only accepts GtkWidgets as children!");

  if (boards_str_equal (type, "body"))
    boards_surface_set_child (self, GTK_WIDGET (child));
  else
    parent_buildable_iface->add_child (buildable, builder, child, type);
}

static void
buildable_iface_init (GtkBuildableIface *iface)
{
  iface->add_child = boards_surface_add_child;

  parent_buildable_iface = g_type_interface_peek_parent (iface);
}


static void
boards_surface_constructed (GObject *object)
{
  BoardsSurface *self = (BoardsSurface *)object;
  BoardsSurfacePrivate *priv = boards_surface_get_instance_private (self);

  G_OBJECT_CLASS (boards_surface_parent_class)->constructed (object);

  priv->click_gesture = (GtkGestureClick *)gtk_gesture_click_new ();
  gtk_gesture_single_set_button (GTK_GESTURE_SINGLE (priv->click_gesture), GDK_BUTTON_SECONDARY);

  gtk_widget_add_controller (GTK_WIDGET (priv->viewport),
                             GTK_EVENT_CONTROLLER (priv->click_gesture));
  g_signal_connect_swapped (priv->click_gesture, "pressed", G_CALLBACK (on_gesture_click_pressed_cb), self);
}

static void
boards_surface_dispose (GObject *object)
{
  BoardsSurface *self = (BoardsSurface *)object;
  BoardsSurfacePrivate *priv = boards_surface_get_instance_private (self);

  gtk_widget_remove_controller (GTK_WIDGET (priv->viewport),
                                GTK_EVENT_CONTROLLER (priv->click_gesture));

  g_clear_pointer ((GtkWidget **)&priv->right_click_menu, gtk_widget_unparent);
  g_clear_pointer ((GtkWidget **)&priv->viewport, gtk_widget_unparent);

  G_OBJECT_CLASS (boards_surface_parent_class)->dispose (object);
}

static void
boards_surface_finalize (GObject *object)
{
  BoardsSurface *self = (BoardsSurface *)object;
  BoardsSurfacePrivate *priv = boards_surface_get_instance_private (self);

  g_clear_pointer (&priv->name, g_free);
  g_clear_pointer (&priv->title, g_free);
  g_clear_pointer (&priv->icon_name, g_free);

  G_OBJECT_CLASS (boards_surface_parent_class)->finalize (object);
}

static void
boards_surface_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  BoardsSurface *self = BOARDS_SURFACE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, boards_surface_get_name (self));
      break;

    case PROP_TITLE:
      g_value_set_string (value, boards_surface_get_title (self));
      break;

    case PROP_ICON_NAME:
      g_value_set_string (value, boards_surface_get_icon_name (self));
      break;

    case PROP_CHILD:
      g_value_set_object (value, boards_surface_get_child (self));
      break;

    case PROP_MENU:
      g_value_set_object (value, boards_surface_get_rc_menu (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_surface_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  BoardsSurface *self = BOARDS_SURFACE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      boards_surface_set_name (self, g_value_get_string (value));
      break;

    case PROP_TITLE:
      boards_surface_set_title (self, g_value_get_string (value));
      break;

    case PROP_ICON_NAME:
      boards_surface_set_icon_name (self, g_value_get_string (value));
      break;

    case PROP_CHILD:
      boards_surface_set_child (self, g_value_get_object (value));
      break;

    case PROP_MENU:
      boards_surface_set_rc_menu (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_surface_class_init (BoardsSurfaceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = boards_surface_constructed;
  object_class->finalize = boards_surface_finalize;
  object_class->dispose = boards_surface_dispose;
  object_class->get_property = boards_surface_get_property;
  object_class->set_property = boards_surface_set_property;

  /**
   * BoardsSurface:name:
   *
   * The "name" property is the name given to the surface
   * for internal use with the Boards Main Window.
   *
   * Since: 0.1
   */
  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the surface.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * BoardsSurface:title:
   *
   * The "title" property is the tile of the surface that will be shown
   * in the omnibar of the Main Window.
   *
   * Since: 0.1
   */
  properties [PROP_TITLE] =
    g_param_spec_string ("title",
                         "Title",
                         "The title of the surface.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * BoardsSurface:icon-name:
   *
   * The "icon-name" property is the icon name of the surface.
   *
   * Since: 0.1
   */
  properties [PROP_ICON_NAME] =
    g_param_spec_string ("icon-name",
                         "Icon Name",
                         "The icon name of the surface.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * BoardsSurface:child:
   *
   * The "child" property is the #GtkWidget child %self.
   *
   * Since: 0.1
   */
  properties [PROP_CHILD] =
    g_param_spec_object ("child",
                         "Child",
                         "The GtkWidget child of the surface.",
                         GTK_TYPE_WIDGET,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * BoardsSurface:menu:
   *
   * The "menu" property is the menu that is going to be shown
   * if the user right-clicked on the %self.
   *
   * Since: 0.1
   */
  properties [PROP_MENU] =
    g_param_spec_object ("menu",
                         "Menu",
                         "The right-click menu of the surface.",
                         G_TYPE_MENU_MODEL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_css_name (widget_class, "viewport");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_set_accessible_role (widget_class, GTK_ACCESSIBLE_ROLE_GROUP);
}

static void
boards_surface_init (BoardsSurface *self)
{
  BoardsSurfacePrivate *priv = boards_surface_get_instance_private (self);

  priv->viewport = g_object_new (GTK_TYPE_SCROLLED_WINDOW,
                                 "hexpand", TRUE,
                                 "vexpand", TRUE,
                                 "hscrollbar-policy", GTK_POLICY_AUTOMATIC,
                                 "vscrollbar-policy", GTK_POLICY_AUTOMATIC,
                                 "has-frame", FALSE,
                                 "propagate-natural-height", TRUE,
                                 "propagate-natural-width", TRUE,
                                 NULL);
  gtk_widget_set_parent (GTK_WIDGET (priv->viewport), GTK_WIDGET (self));
  gtk_widget_set_focus_child (GTK_WIDGET (self), GTK_WIDGET (priv->viewport));

  priv->right_click_menu = g_object_new (GTK_TYPE_POPOVER_MENU,
                                         "autohide", TRUE,
                                         "has-arrow", FALSE,
                                         NULL);
  gtk_widget_set_parent (GTK_WIDGET (priv->right_click_menu), GTK_WIDGET (self));
  gtk_widget_set_halign (GTK_WIDGET (priv->right_click_menu), GTK_ALIGN_START);
}


/**
 * boards_surface_get_name:
 * @self: a #BoardsSurface.
 *
 * Gets the name property of @self.
 *
 * Returns: (transfer full): a #gchar.
 *
 * Since: 0.1
 */
gchar *
boards_surface_get_name (BoardsSurface *self)
{
  BoardsSurfacePrivate *priv = boards_surface_get_instance_private (self);

  g_return_val_if_fail (BOARDS_IS_SURFACE (self), NULL);

  return priv->name;
}

/**
 * boards_surface_get_title:
 * @self: a #BoardsSurface.
 *
 * Gets the title property of @self.
 *
 * Returns: (transfer full): a #gchar.
 *
 * Since: 0.1
 */
gchar *
boards_surface_get_title (BoardsSurface *self)
{
  BoardsSurfacePrivate *priv = boards_surface_get_instance_private (self);

  g_return_val_if_fail (BOARDS_IS_SURFACE (self), NULL);

  return priv->title;
}

/**
 * boards_surface_set_title:
 * @self: a #BoardsSurface.
 * @title: a #gchar.
 *
 * Sets the title property of @self.
 *
 * Since: 0.1
 */
void
boards_surface_set_title (BoardsSurface *self,
                          const gchar   *title)
{
  BoardsSurfacePrivate *priv = boards_surface_get_instance_private (self);

  g_return_if_fail (BOARDS_IS_SURFACE (self));
  g_return_if_fail (title);

  BOARDS_ENTRY;

  if (!boards_str_equal (priv->title, title))
    {
      g_free (priv->title);
      priv->title = g_strdup (title);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TITLE]);
    }

  BOARDS_EXIT;
}

/**
 * boards_surface_get_icon_name:
 * @self: a #BoardsSurface.
 *
 * Gets the icon-name property of @self.
 *
 * Returns: (transfer full): a #gchar.
 *
 * Since: 0.1
 */
gchar *
boards_surface_get_icon_name (BoardsSurface *self)
{
  BoardsSurfacePrivate *priv = boards_surface_get_instance_private (self);

  g_return_val_if_fail (BOARDS_IS_SURFACE (self), NULL);

  return priv->icon_name;
}

/**
 * boards_surface_set_icon_name:
 * @self: a #BoardsSurface.
 * @icon_name: a #gchar.
 *
 * Sets the icon-name property of @self.
 *
 * Since: 0.1
 */
void
boards_surface_set_icon_name (BoardsSurface *self,
                              const gchar   *icon_name)
{
  BoardsSurfacePrivate *priv = boards_surface_get_instance_private (self);

  g_return_if_fail (BOARDS_IS_SURFACE (self));
  g_return_if_fail (icon_name);

  BOARDS_ENTRY;

  if (!boards_str_equal (priv->icon_name, icon_name))
    {
      g_free (priv->icon_name);
      priv->icon_name = g_strdup (icon_name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ICON_NAME]);
    }

  BOARDS_EXIT;
}

/**
 * boards_surface_get_child:
 * @self: a #BoardsSurface.
 *
 * Gets the child property of @self.
 *
 * Returns: (transfer full): a #GtkWidget.
 *
 * Since: 0.1
 */
GtkWidget *
boards_surface_get_child (BoardsSurface *self)
{
  BoardsSurfacePrivate *priv = boards_surface_get_instance_private (self);

  g_return_val_if_fail (BOARDS_IS_SURFACE (self), NULL);

  return gtk_scrolled_window_get_child (priv->viewport);
}

/**
 * boards_surface_set_child:
 * @self: a #BoardsSurface.
 * @child: a #GtkWidget.
 *
 * Sets the child property of @self.
 *
 * Since: 0.1
 */
void
boards_surface_set_child (BoardsSurface *self,
                          GtkWidget     *child)
{
  BoardsSurfacePrivate *priv = boards_surface_get_instance_private (self);

  g_return_if_fail (BOARDS_IS_SURFACE (self));
  g_return_if_fail (GTK_IS_WIDGET (child));

  BOARDS_ENTRY;

  if (child == gtk_scrolled_window_get_child (priv->viewport))
    BOARDS_EXIT;

  gtk_scrolled_window_set_child (priv->viewport, child);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CHILD]);
  BOARDS_EXIT;
}

/**
 * boards_surface_get_rc_menu:
 * @self: a #BoardsSurface.
 *
 * Gets the menu property of @self.
 *
 * Returns: (transfer full): a #gchar.
 *
 * Since: 0.1
 */
GMenuModel *
boards_surface_get_rc_menu (BoardsSurface *self)
{
  BoardsSurfacePrivate *priv = boards_surface_get_instance_private (self);

  g_return_val_if_fail (BOARDS_IS_SURFACE (self), NULL);

  return gtk_popover_menu_get_menu_model (priv->right_click_menu);
}

/**
 * boards_surface_set_rc_menu:
 * @self: a #BoardsSurface.
 * @menu: a #GMenuModel.
 *
 * Sets the menu property of @self.
 *
 * Since: 0.1
 */
void
boards_surface_set_rc_menu (BoardsSurface *self,
                            GMenuModel    *menu)
{
  BoardsSurfacePrivate *priv = boards_surface_get_instance_private (self);

  g_return_if_fail (BOARDS_IS_SURFACE (self));
  g_return_if_fail (G_IS_MENU_MODEL (menu));

  BOARDS_ENTRY;

  if (menu == gtk_popover_menu_get_menu_model (priv->right_click_menu))
    BOARDS_EXIT;

  gtk_popover_menu_set_menu_model (priv->right_click_menu, menu);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_MENU]);
  BOARDS_EXIT;
}

/**
 * boards_surface_set:
 * @self: a #BoardsSurface.
 *
 * This function should be called only from main_window.add_surface()
 * function after the surface is attached.
 *
 * Since: 0.1
 */
void
boards_surface_set (BoardsSurface *self)
{
  g_return_if_fail (BOARDS_IS_SURFACE (self));

  if (BOARDS_SURFACE_GET_CLASS (self)->surface_set)
    BOARDS_SURFACE_GET_CLASS (self)->surface_set (self);
}

/**
 * boards_surface_unset:
 * @self: a #BoardsSurface.
 *
 * This function should be called only from main_window.close_surface()
 * function before the surface is going to be dettached.
 *
 * Since: 0.1
 */
void
boards_surface_unset (BoardsSurface *self)
{
  g_return_if_fail (BOARDS_IS_SURFACE (self));

  if (BOARDS_SURFACE_GET_CLASS (self)->surface_unset)
    BOARDS_SURFACE_GET_CLASS (self)->surface_unset (self);
}


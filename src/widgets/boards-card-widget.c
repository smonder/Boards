/* boards-card-widget.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "boards-card-widget"

#include "config.h"
#include "boards-debug.h"

#include <glib/gi18n.h>

#include "boards-card-widget.h"

struct _BoardsCardWidget
{
  GtkWidget       parent_instance;

  BoardsCard *    card;

  AdwClamp *      body;
  AdwEntryRow *   card_title;
  AdwComboRow *   card_state;
  GtkCalendar *   card_date;
  GtkLabel *      card_date_label;
  GtkTextView *   card_body;
};

G_DEFINE_FINAL_TYPE (BoardsCardWidget, boards_card_widget, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_CARD,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


static void
on_edit_memo_button_toggled_cb (BoardsCardWidget *self,
                                GtkToggleButton  *edit_memo)
{
  gboolean state;
  GtkTextBuffer *buffer;
  const gchar *text;
  GtkWidget *button_content;

  g_assert (BOARDS_IS_CARD_WIDGET (self));
  g_assert (GTK_IS_TOGGLE_BUTTON (edit_memo));

  BOARDS_ENTRY;
  state = gtk_toggle_button_get_active (edit_memo);

  button_content = gtk_button_get_child (GTK_BUTTON (edit_memo));
  g_assert (ADW_IS_BUTTON_CONTENT (button_content));

  gtk_text_view_set_editable (self->card_body, state);
  gtk_widget_set_focusable (GTK_WIDGET (self->card_body), state);

  if (state)
    {
      gtk_text_view_set_cursor_visible (self->card_body, state);
      gtk_widget_grab_focus (GTK_WIDGET (self->card_body));

      adw_button_content_set_label (ADW_BUTTON_CONTENT (button_content), _("Save Changes"));
      adw_button_content_set_icon_name (ADW_BUTTON_CONTENT (button_content),
                                        "document-save-symbolic");
      gtk_widget_add_css_class (GTK_WIDGET (edit_memo), "suggested-action");
      gtk_widget_remove_css_class (GTK_WIDGET (edit_memo), "flat");
      BOARDS_EXIT;
    }

  buffer = gtk_text_view_get_buffer (self->card_body);
  g_object_get (buffer , "text", &text, NULL);
  boards_card_set_desc (self->card, text);

  adw_button_content_set_label (ADW_BUTTON_CONTENT (button_content), _("Edit"));
  adw_button_content_set_icon_name (ADW_BUTTON_CONTENT (button_content),
                                    "document-edit-symbolic");
  gtk_widget_add_css_class (GTK_WIDGET (edit_memo), "flat");
  gtk_widget_remove_css_class (GTK_WIDGET (edit_memo), "suggested-action");
  gtk_text_buffer_set_modified (buffer, FALSE);

  BOARDS_EXIT;
}

static void
on_card_date_day_selected_cb (BoardsCardWidget *self,
                              GtkCalendar      *cal)
{
  GDateTime *dt;
  const gchar *time_format;

  g_assert (BOARDS_IS_CARD_WIDGET (self));
  g_assert (GTK_IS_CALENDAR (cal));

  dt = gtk_calendar_get_date (cal);
  time_format = g_date_time_format (dt, "%e/%m/%Y");

  boards_card_set_date (self->card, dt);
  gtk_label_set_label (self->card_date_label, time_format);
}

static void
boards_card_widget_finalize (GObject *object)
{
  BoardsCardWidget *self = (BoardsCardWidget *)object;

  g_clear_weak_pointer (&self->card);

  G_OBJECT_CLASS (boards_card_widget_parent_class)->finalize (object);
}

static void
boards_card_widget_dispose (GObject *object)
{
  BoardsCardWidget *self = (BoardsCardWidget *)object;
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (self))))
    g_clear_pointer (&child, gtk_widget_unparent);

  G_OBJECT_CLASS (boards_card_widget_parent_class)->dispose (object);
}

static void
boards_card_widget_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  BoardsCardWidget *self = BOARDS_CARD_WIDGET (object);

  switch (prop_id)
    {
    case PROP_CARD:
      g_value_set_object (value, boards_card_widget_get_card (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_card_widget_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  BoardsCardWidget *self = BOARDS_CARD_WIDGET (object);

  switch (prop_id)
    {
    case PROP_CARD:
      boards_card_widget_set_card (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_card_widget_class_init (BoardsCardWidgetClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = boards_card_widget_dispose;
  object_class->finalize = boards_card_widget_finalize;
  object_class->get_property = boards_card_widget_get_property;
  object_class->set_property = boards_card_widget_set_property;

  properties [PROP_CARD] =
    g_param_spec_object ("card",
                         "Card",
                         "The BoardsCard object to link with.",
                         BOARDS_TYPE_CARD,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_set_accessible_role (widget_class, GTK_ACCESSIBLE_ROLE_FORM);
  gtk_widget_class_set_template_from_resource (widget_class, "/ui/boards-card-widget.ui");

  gtk_widget_class_bind_template_child (widget_class, BoardsCardWidget, body);
  gtk_widget_class_bind_template_child (widget_class, BoardsCardWidget, card_title);
  gtk_widget_class_bind_template_child (widget_class, BoardsCardWidget, card_state);
  gtk_widget_class_bind_template_child (widget_class, BoardsCardWidget, card_date);
  gtk_widget_class_bind_template_child (widget_class, BoardsCardWidget, card_date_label);
  gtk_widget_class_bind_template_child (widget_class, BoardsCardWidget, card_body);

  gtk_widget_class_bind_template_callback (widget_class, on_edit_memo_button_toggled_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_card_date_day_selected_cb);
}

static void
boards_card_widget_init (BoardsCardWidget *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}


/**
 * boards_card_widget_new:
 *
 * Creates a new #BoardsCardWidget widget.
 *
 * Returns: a newly created #GtkWidget.
 *
 * Since: 0.1
 */
GtkWidget *
boards_card_widget_new (void)
{
  return g_object_new (BOARDS_TYPE_CARD_WIDGET, NULL);
}

BoardsCard *
boards_card_widget_get_card (BoardsCardWidget *self)
{
  g_return_val_if_fail (BOARDS_IS_CARD_WIDGET (self), NULL);

  return self->card;
}

static gboolean
state_to_adw_enum (GBinding     *binding,
                   const GValue *from_value,
                   GValue       *to_value,
                   gpointer      user_data)
{
  BoardsState state;
  AdwComboRow *row = user_data;

  g_assert (G_VALUE_HOLDS_ENUM (from_value));
  g_assert (ADW_IS_COMBO_ROW (row));

  state = g_value_get_enum (from_value);
  g_value_set_uint (to_value,
                    adw_enum_list_model_find_position (
                    ADW_ENUM_LIST_MODEL (adw_combo_row_get_model (row)), state));

  return TRUE;
}

static gboolean
adw_enum_to_state (GBinding     *binding,
                   const GValue *from_value,
                   GValue       *to_value,
                   gpointer      user_data)
{
  g_autoptr (AdwEnumListItem) item = NULL;
  BoardsState state;
  AdwComboRow *row = user_data;

  g_assert (G_VALUE_HOLDS_UINT (from_value));
  g_assert (ADW_IS_COMBO_ROW (row));

  item = g_list_model_get_item (adw_combo_row_get_model (row),
                                g_value_get_uint (from_value));

  state = adw_enum_list_item_get_value (item);
  g_value_set_enum (to_value, state);

  return TRUE;
}

void
boards_card_widget_set_card (BoardsCardWidget *self,
                             BoardsCard       *card)
{
  g_return_if_fail (BOARDS_IS_CARD_WIDGET (self));
  g_return_if_fail (BOARDS_IS_CARD (card));

  BOARDS_ENTRY;
  if (self->card == card)
    BOARDS_EXIT;

  if (g_set_weak_pointer (&self->card, card))
    {
      g_object_bind_property (self->card, "title",
                              self->card_title, "text",
                              G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
      g_object_bind_property (self->card, "desc",
                              gtk_text_view_get_buffer (self->card_body), "text",
                              G_BINDING_SYNC_CREATE);
      g_object_bind_property_full (self->card, "state",
                                   self->card_state, "selected",
                                   G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                                   state_to_adw_enum,
                                   adw_enum_to_state, self->card_state, NULL);

      gtk_calendar_select_day (self->card_date,
                               boards_card_get_date (self->card));

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CARD]);
    }
  BOARDS_EXIT;
}

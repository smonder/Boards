/* boards-dialog.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <adwaita.h>
#include "boards-version-macros.h"

G_BEGIN_DECLS

#define BOARDS_TYPE_DIALOG (boards_dialog_get_type())
#define boards_dialog_destroy(dlg) \
          gtk_window_destroy(GTK_WINDOW (dlg));

BOARDS_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (BoardsDialog, boards_dialog, BOARDS, DIALOG, AdwWindow)

BOARDS_AVAILABLE_IN_ALL
BoardsDialog *  boards_dialog_new                (void) G_GNUC_WARN_UNUSED_RESULT;
BOARDS_AVAILABLE_IN_ALL
BoardsDialog *  boards_dialog_new_for_content    (const gchar *  title,
                                                  GtkWidget *    content) G_GNUC_WARN_UNUSED_RESULT;

BOARDS_AVAILABLE_IN_ALL
GtkWidget *     boards_dialog_get_body           (BoardsDialog * self);
BOARDS_AVAILABLE_IN_ALL
void            boards_dialog_set_body           (BoardsDialog * self,
                                                  GtkWidget *    body);
BOARDS_AVAILABLE_IN_ALL
void            boards_dialog_add_header_suffix  (BoardsDialog * self,
                                                  GtkWidget *    child);
BOARDS_AVAILABLE_IN_ALL
void            boards_dialog_add_header_prefix  (BoardsDialog * self,
                                                  GtkWidget *    child);

BOARDS_AVAILABLE_IN_ALL
void            boards_dialog_run                (BoardsDialog * self);



G_END_DECLS

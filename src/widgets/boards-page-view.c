/* boards-page-view.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "boards-page-view.h"

#include "config.h"
#include "boards-debug.h"

#include "boards-application.h"
#include "boards-dialog.h"
#include "boards-list-editor.h"
#include "boards-page-view-private.h"

G_DEFINE_FINAL_TYPE (BoardsPageView, boards_page_view, BOARDS_TYPE_SURFACE)

enum {
  PROP_0,
  PROP_PAGE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


static gboolean
on_boards_page_view_drop_cb (GtkDropTarget *target,
                             const GValue  *value,
                             double         x,
                             double         y,
                             gpointer       user_data)
{
  BoardsPageView *self = (BoardsPageView *)user_data;
  guint position;

  g_assert (BOARDS_IS_PAGE_VIEW (self));

  /* Call the appropriate setter depending on the type of data
   * that we received
   */
  if (!G_VALUE_HOLDS (value, BOARDS_TYPE_LIST))
    return FALSE;

  position = _boards_list_view_get_index (_boards_page_view_get_list_at_x (self, x));

  if (boards_page_get_n_lists (self->page) == 0 ||
      position >= boards_page_get_n_lists (self->page))
    {
      boards_page_append_list (self->page, g_value_get_object (value));
      return TRUE;
    }

  boards_page_insert_list (self->page, g_value_get_object (value), position);

  return TRUE;
}

static GdkDragAction
on_boards_page_view_motion_cb (GtkDropTarget *target,
                               double         x,
                               double         y,
                               gpointer       user_data)
{
  BoardsPageView *self = (BoardsPageView *)user_data;

  g_assert (BOARDS_IS_PAGE_VIEW (self));

  _boards_page_view_highlight_list (self, _boards_page_view_get_list_at_x (self, x));

  return GDK_ACTION_MOVE;
}

static void
on_boards_page_view_leave_cb (GtkDropTarget *target,
                              gpointer       user_data)
{
  BoardsPageView *self = (BoardsPageView *)user_data;

  g_assert (BOARDS_IS_PAGE_VIEW (self));

  _boards_page_view_unhighlight_list (self);
}


static void
on_remove_list_cb (BoardsPageView *self,
                   BoardsListView *list_view)
{
  g_assert (BOARDS_IS_PAGE_VIEW (self));
  g_assert (BOARDS_IS_LIST_VIEW (list_view));

  boards_page_delete_list (self->page,
                           boards_list_view_get_list (list_view));
}

static void
boards_page_view_lists_changed (GListModel *model,
                                guint       position,
                                guint       removed,
                                guint       added,
                                gpointer    user_data)
{
  BoardsPageView *self = user_data;

  while (removed --)
    {
      BoardsListView *list;
      list = _boards_page_view_get_list_at_index (self, position);
      _boards_page_view_remove_list (self, list);
    }

  for (guint i = 0; i < added; i++)
    {
      BoardsList *list;
      BoardsListView *list_view;

      list = g_list_model_get_item (model, position + i);
      list_view = (BoardsListView *)boards_list_view_new ();
      boards_list_view_set_list (list_view, list);
      g_signal_connect_swapped (list_view, "remove-list", G_CALLBACK (on_remove_list_cb), self);
      _boards_page_view_insert_list (self, list_view, position + i);

      g_object_unref (list);
    }
}

static void
boards_page_view_set_page (BoardsPageView *self,
                           BoardsPage    *page)
{
  GSequenceIter *iter;
  g_return_if_fail (BOARDS_IS_PAGE_VIEW (self));
  g_return_if_fail (BOARDS_IS_PAGE (page));

  BOARDS_ENTRY;

  if (self->page == page)
    BOARDS_EXIT;

  if (self->page)
    {
      g_signal_handlers_disconnect_by_func (boards_page_get_lists (self->page),
                                            boards_page_view_lists_changed, self);
      g_clear_object (&self->page);
    }

  iter = g_sequence_get_begin_iter (self->lists);
  while (!g_sequence_iter_is_end (iter))
    {
      BoardsListView *list = g_sequence_get (iter);
      iter = g_sequence_iter_next (iter);
      _boards_page_view_remove_list (self, list);
    }

  if (g_set_object (&self->page, page))
    {
      g_object_bind_property (self->page, "title",
                              self, "title",
                              G_BINDING_SYNC_CREATE);

      g_signal_connect (boards_page_get_lists (self->page),
                        "items-changed", G_CALLBACK (boards_page_view_lists_changed),
                        self);
      boards_page_view_lists_changed (boards_page_get_lists (self->page), 0, 0,
                                      boards_page_get_n_lists (self->page), self);

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PAGE]);
    }

  BOARDS_EXIT;
}

#if 0
static void
msg_dialog_response_cb (BoardsPageView   *self,
                        gchar            *response,
                        AdwMessageDialog *msg_dlg)
{
  g_assert (BOARDS_IS_PAGE_VIEW (self));
  g_assert (ADW_IS_MESSAGE_DIALOG (msg_dlg));

  if (boards_str_equal (response, "yes"))
    _boards_page_view_queue_save (self);
}

static void
boards_page_view_surface_unset (BoardsSurface *surface)
{
  BoardsPageView *self = (BoardsPageView *)surface;
  GtkWidget *dialog;

  BOARDS_ENTRY;

  if (boards_page_is_modified (self->page))
    {
      dialog = adw_message_dialog_new ((GtkWindow *)boards_application_get_current_window (BOARDS_APPLICATION_DEFAULT),
                                       _("Unsaved Changes"), NULL);

      adw_message_dialog_format_body (ADW_MESSAGE_DIALOG (dialog),
                                      _("You made some changes on <%s> that are going to be lost,\nDo you want to save?"),
                                      boards_page_get_title (self->page));

      adw_message_dialog_add_responses (ADW_MESSAGE_DIALOG (dialog),
                                        "no",  _("_No"),
                                        "yes", _("_Yes"),
                                        NULL);

      adw_message_dialog_set_response_appearance (ADW_MESSAGE_DIALOG (dialog),
                                                  "no", ADW_RESPONSE_DESTRUCTIVE);

      adw_message_dialog_set_default_response (ADW_MESSAGE_DIALOG (dialog), "yes");
      adw_message_dialog_set_close_response (ADW_MESSAGE_DIALOG (dialog), "no");

      g_signal_connect_swapped (dialog, "response", G_CALLBACK (msg_dialog_response_cb), self);
      gtk_window_present (GTK_WINDOW (dialog));
    }
  BOARDS_EXIT;
}
#endif

static void
boards_page_view_finalize (GObject *object)
{
  BoardsPageView *self = (BoardsPageView *)object;

  g_signal_handlers_disconnect_by_func (boards_page_get_lists (self->page),
                                        boards_page_view_lists_changed, self);
  g_clear_object (&self->page);
  g_sequence_free (self->lists);

  G_OBJECT_CLASS (boards_page_view_parent_class)->finalize (object);
}

static void
boards_page_view_dispose (GObject *object)
{
  BoardsPageView *self = (BoardsPageView *)object;

  g_clear_pointer ((GtkWidget **)&self->lists_box, gtk_widget_unparent);

  G_OBJECT_CLASS (boards_page_view_parent_class)->dispose (object);
}

static void
boards_page_view_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  BoardsPageView *self = BOARDS_PAGE_VIEW (object);

  switch (prop_id)
    {
    case PROP_PAGE:
      g_value_set_object (value, boards_page_view_get_page (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_page_view_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  BoardsPageView *self = BOARDS_PAGE_VIEW (object);

  switch (prop_id)
    {
    case PROP_PAGE:
      boards_page_view_set_page (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_page_view_class_init (BoardsPageViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  /* BoardsSurfaceClass *surface_class = BOARDS_SURFACE_CLASS (klass); */

  object_class->dispose = boards_page_view_dispose;
  object_class->finalize = boards_page_view_finalize;
  object_class->get_property = boards_page_view_get_property;
  object_class->set_property = boards_page_view_set_property;

  /* surface_class->surface_unset = boards_page_view_surface_unset; */

  /**
   * BoardsPageView:page:
   *
   * The "page" property is the #BoardsPage that is bound to %self.
   * each #BoardsPageView object is created for one #BoardsPage.
   *
   * Since: 0.1
   */
  properties [PROP_PAGE] =
    g_param_spec_object ("page",
                         "Page",
                         "The BoardsPage to link with.",
                         BOARDS_TYPE_PAGE,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  g_type_ensure (BOARDS_TYPE_LIST_VIEW);

  gtk_widget_class_set_template_from_resource (widget_class, "/ui/boards-page-view.ui");
  gtk_widget_class_bind_template_child (widget_class, BoardsPageView, lists_box);
}

static void
boards_page_view_init (BoardsPageView *self)
{
  GtkDropTarget *drop_target;

  gtk_widget_init_template (GTK_WIDGET (self));
  self->lists = g_sequence_new (NULL);

  drop_target = gtk_drop_target_new (BOARDS_TYPE_CARD, GDK_ACTION_MOVE);
  gtk_event_controller_set_propagation_phase (GTK_EVENT_CONTROLLER (drop_target), GTK_PHASE_CAPTURE);
  gtk_drop_target_set_gtypes (drop_target, (GType [1]) { BOARDS_TYPE_LIST }, 1);
  g_signal_connect (drop_target, "drop", G_CALLBACK (on_boards_page_view_drop_cb), self);
  g_signal_connect (drop_target, "motion", G_CALLBACK (on_boards_page_view_motion_cb), self);
  g_signal_connect (drop_target, "leave", G_CALLBACK (on_boards_page_view_leave_cb), self);
  gtk_widget_add_controller (GTK_WIDGET (self), GTK_EVENT_CONTROLLER (drop_target));
}


/**
 * boards_page_view_new:
 * @page: a #BoardsPage: the page that this view is going to show.
 *
 * Creates a new #BoardsPageView.
 *
 * Returns: (transfer full) (nullable): A newly created #BoardsSurface.
 *
 * Since: 0.1
 */
BoardsSurface *
boards_page_view_new (BoardsPage * page)
{
  g_return_val_if_fail (BOARDS_IS_PAGE (page), NULL);

  return g_object_new (BOARDS_TYPE_PAGE_VIEW,
                       "name", boards_page_get_name (page),
                       "title", boards_page_get_title (page),
                       "icon-name", "computer-apple-ipad-symbolic",
                       "page", page,
                       NULL);
}

BoardsPage *
boards_page_view_get_page (BoardsPageView *self)
{
  g_return_val_if_fail (BOARDS_IS_PAGE_VIEW (self), NULL);

  return self->page;
}


BoardsListView *
_boards_page_view_get_list_at_index (BoardsPageView *self,
                                     gint            index_)
{
  GSequenceIter *iter;

  g_return_val_if_fail (BOARDS_IS_PAGE_VIEW (self), NULL);

  iter = g_sequence_get_iter_at_pos (self->lists, index_);
  if (!g_sequence_iter_is_end (iter))
    return g_sequence_get (iter);

  return NULL;
}

void
_boards_page_view_remove_list (BoardsPageView *self,
                               BoardsListView *list)
{
  gboolean was_visible;
  GSequenceIter *iter;

  g_return_if_fail (BOARDS_IS_PAGE_VIEW (self));
  g_return_if_fail (BOARDS_IS_LIST_VIEW (list));

  BOARDS_ENTRY;

  was_visible = gtk_widget_get_visible (GTK_WIDGET (list));

  iter = _boards_list_view_get_iter (list);
  if (g_sequence_iter_get_sequence (iter) != self->lists)
    {
      g_warning ("Tried to remove non-child %p", list);
      return;
    }

  if (self->highlighted_list == list)
    _boards_page_view_unhighlight_list (self);

  gtk_box_remove (self->lists_box, GTK_WIDGET (list));
  g_sequence_remove (iter);

  iter = NULL;
  list = NULL;

  if (was_visible && gtk_widget_get_visible (GTK_WIDGET (self)))
    gtk_widget_queue_resize (GTK_WIDGET (self));

  BOARDS_EXIT;
}



static GdkContentProvider *
on_boards_list_view_drag_prepare_cb (GtkDragSource  *source,
                                     double          x,
                                     double          y,
                                     BoardsListView *list)
{
  g_assert (BOARDS_IS_LIST_VIEW (list));

  return gdk_content_provider_new_union ((GdkContentProvider *[1]) {
      gdk_content_provider_new_typed (BOARDS_TYPE_LIST,
                                      boards_list_view_get_list (list)),
    }, 1);
}

static void
on_boards_list_view_drag_begin_cb (GtkDragSource  *source,
                                   GdkDrag        *drag,
                                   BoardsListView *list)
{
  GdkPaintable *paintable;
  g_assert (BOARDS_IS_LIST_VIEW (list));

  /* Set the widget as the drag icon */
  paintable = gtk_widget_paintable_new (GTK_WIDGET (list));
  gtk_drag_source_set_icon (source, paintable, 0, 0);
  g_object_unref (paintable);
}

static void
on_boards_list_view_drag_end_cb (GtkDragSource    *source,
                                 GdkDrag          *drag,
                                 gboolean          delete_data,
                                 PageViewDragData *drag_data)
{
  g_assert (BOARDS_IS_PAGE_VIEW (drag_data->page_view));
  g_assert (BOARDS_IS_LIST_VIEW (drag_data->list_view));

  if (delete_data)
    {
      boards_page_delete_list (drag_data->page_view->page,
                               boards_list_view_get_list (drag_data->list_view));
    }
}



void
_boards_page_view_insert_list (BoardsPageView *self,
                               BoardsListView *list,
                               const gint      position)
{
  GSequenceIter *prev = NULL;
  GSequenceIter *iter = NULL;
  GtkDragSource *drag_source;
  PageViewDragData *drag_data = g_new0 (PageViewDragData, 1);

  g_return_if_fail (BOARDS_IS_PAGE_VIEW (self));
  g_return_if_fail (BOARDS_IS_LIST_VIEW (list));

  BOARDS_ENTRY;

  if (position == 0)
    iter = g_sequence_prepend (self->lists, list);
  else if (position == -1)
    iter = g_sequence_append (self->lists, list);
  else
    {
      GSequenceIter *current_iter;

      current_iter = g_sequence_get_iter_at_pos (self->lists, position);
      iter = g_sequence_insert_before (current_iter, list);
    }

  _boards_list_view_set_iter (list, iter);
  prev = g_sequence_iter_prev (iter);
  gtk_box_insert_child_after (self->lists_box,
                              GTK_WIDGET (list),
                              (prev != iter)? g_sequence_get (prev) : NULL);

  drag_source = gtk_drag_source_new ();
  gtk_drag_source_set_actions (drag_source, GDK_ACTION_MOVE);
  gtk_event_controller_set_propagation_phase (GTK_EVENT_CONTROLLER (drag_source), GTK_PHASE_CAPTURE);
  g_signal_connect (drag_source, "prepare", G_CALLBACK (on_boards_list_view_drag_prepare_cb), list);
  g_signal_connect (drag_source, "drag-begin", G_CALLBACK (on_boards_list_view_drag_begin_cb), list);

  drag_data->page_view = self;
  drag_data->list_view = list;
  g_signal_connect (drag_source, "drag-end", G_CALLBACK (on_boards_list_view_drag_end_cb), drag_data);
  gtk_widget_add_controller (_boards_list_view_get_drag_widget (list), GTK_EVENT_CONTROLLER (drag_source));

  BOARDS_EXIT;
}

void
_boards_page_view_highlight_list (BoardsPageView *self,
                                  BoardsListView *list)
{
  g_return_if_fail (BOARDS_IS_PAGE_VIEW (self));
  g_return_if_fail (BOARDS_IS_LIST_VIEW (list));

  if (self->highlighted_list == list)
    return;

  _boards_page_view_unhighlight_list (self);
  gtk_widget_set_state_flags (GTK_WIDGET (list), GTK_STATE_FLAG_DROP_ACTIVE, FALSE);
  g_set_object (&self->highlighted_list, list);
}

void
_boards_page_view_unhighlight_list (BoardsPageView *self)
{
  g_return_if_fail (BOARDS_IS_PAGE_VIEW (self));

  if (self->highlighted_list == NULL)
    return;

  gtk_widget_unset_state_flags (GTK_WIDGET (self->highlighted_list), GTK_STATE_FLAG_DROP_ACTIVE);
  g_clear_object (&self->highlighted_list);
}

static gint
list_x_cmp_func (gconstpointer a,
                 gconstpointer b,
                 gpointer      user_data)
{
  BoardsPageView *self = user_data;
  BoardsListView *list_view = (BoardsListView *)a;
  int x = GPOINTER_TO_INT (b);

  g_assert (BOARDS_IS_PAGE_VIEW (self));
  g_assert (BOARDS_IS_LIST_VIEW (list_view));

  if (x < _boards_list_view_get_x (list_view))
    return 1;
  else if (x >= _boards_list_view_get_x (list_view) +
                _boards_list_view_get_width (list_view) +
                gtk_box_get_spacing (self->lists_box))
    return -1;

  return 0;
}

BoardsListView *
_boards_page_view_get_list_at_x (BoardsPageView *self,
                                 gint            x)
{
  GSequenceIter *iter;

  g_return_val_if_fail (BOARDS_IS_PAGE_VIEW (self), NULL);

  _boards_page_view_queue_update (self);

  iter = g_sequence_lookup (self->lists,
                            GINT_TO_POINTER (x),
                            list_x_cmp_func, self);

  if (iter)
    return BOARDS_LIST_VIEW (g_sequence_get (iter));

  return NULL;
}

void
_boards_page_view_queue_update (BoardsPageView *self)
{
  GtkAllocation child_allocation;
  GSequenceIter *iter;
  BoardsListView *list;
  gint child_min;

  child_allocation.x = 0;
  child_allocation.y = 0;
  child_allocation.width = 0;
  child_allocation.height = gtk_widget_get_size (GTK_WIDGET (self), GTK_ORIENTATION_VERTICAL);

  for (iter = g_sequence_get_begin_iter (self->lists);
       !g_sequence_iter_is_end (iter);
       iter = g_sequence_iter_next (iter))
    {
      list = g_sequence_get (iter);

      _boards_list_view_set_x (list, child_allocation.x);
      gtk_widget_measure (GTK_WIDGET (list), GTK_ORIENTATION_HORIZONTAL,
                          child_allocation.height,
                          &child_min, NULL, NULL, NULL);
      child_allocation.width = child_min;
      _boards_list_view_set_width (list, child_allocation.width);
      gtk_widget_size_allocate (GTK_WIDGET (list), &child_allocation, -1);
      child_allocation.x += child_min + gtk_box_get_spacing (self->lists_box);
    }
}


static void
on_save_page_dlg_response_cb (BoardsPageView *self,
                              gint            response,
                              GtkDialog      *save_dlg)
{
  g_assert (BOARDS_IS_PAGE_VIEW (self));
  g_assert (GTK_IS_DIALOG (save_dlg));

  BOARDS_ENTRY;
  if (response == GTK_RESPONSE_ACCEPT)
    {
      GtkFileChooser *chooser = GTK_FILE_CHOOSER (save_dlg);
      g_autoptr (GFile) page_file = gtk_file_chooser_get_file (chooser);
      const gchar * choice = gtk_file_chooser_get_choice (chooser, "encoding");
      GError *error = NULL;

      if (boards_str_equal (choice, "xmlfile"))
        {
          if (!boards_page_save_to_file (self->page, page_file, NULL, &error))
            g_critical ("Error saving page: %s", error->message);
          else
            boards_page_set_file (self->page, g_steal_pointer (&page_file));
        }
    }

  gtk_window_destroy (GTK_WINDOW (save_dlg));
  BOARDS_EXIT;
}

void
_boards_page_view_queue_save (BoardsPageView *self)
{
  g_autoptr (GFile) file = NULL;
  GtkWidget *save_page_dlg;
  g_autoptr (GtkFileFilter) file_filter = NULL;
  static const char *options[] = { "xmlfile", NULL };
  static const char *options_labels[] = { "XML File Encoding", NULL };
  GError *error = NULL;

  g_return_if_fail (BOARDS_IS_PAGE_VIEW (self));

  BOARDS_ENTRY;
  file = boards_page_get_file (self->page);

  if (!file)
    {
      save_page_dlg = gtk_file_chooser_dialog_new (_("Save Page"),
                                                   (GtkWindow *)boards_application_get_current_window (BOARDS_APPLICATION_DEFAULT),
                                                   GTK_FILE_CHOOSER_ACTION_SAVE,
                                                   _("_Cancel"), GTK_RESPONSE_CANCEL,
                                                   _("_Save"), GTK_RESPONSE_ACCEPT,
                                                   NULL);

      g_signal_connect_swapped (save_page_dlg, "response",
                                G_CALLBACK (on_save_page_dlg_response_cb), self);
      gtk_file_chooser_add_choice (GTK_FILE_CHOOSER (save_page_dlg),
                                   "encoding", "File Enconding",
                                   options, options_labels);

      file_filter = gtk_file_filter_new ();
      gtk_file_filter_set_name (file_filter, "Boards Page");
      gtk_file_filter_add_suffix (file_filter, "board");
      gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (save_page_dlg), file_filter);

      gtk_widget_show (save_page_dlg);
      BOARDS_EXIT;
    }

  if (!boards_page_save_to_file (self->page,
                                 boards_page_get_file (self->page),
                                 NULL, &error))
    g_critical ("Error saving page: %s", error->message);

  BOARDS_EXIT;
}


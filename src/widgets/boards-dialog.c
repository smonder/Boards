/* boards-dialog.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "boards-dialog"

#include "config.h"
#include "boards-debug.h"

#include "boards-application.h"
#include "boards-dialog.h"
#include "boards-macros.h"

struct _BoardsDialog
{
  AdwWindow          parent_instance;

  GtkBox *           box;
  AdwHeaderBar *     hbar;

  AdwBin *           body;
};

static void buildable_iface_init (GtkBuildableIface *iface);

G_DEFINE_FINAL_TYPE_WITH_CODE (BoardsDialog, boards_dialog, ADW_TYPE_WINDOW,
                               G_IMPLEMENT_INTERFACE (GTK_TYPE_BUILDABLE, buildable_iface_init))

enum {
  PROP_0,
  PROP_BODY,
  N_PROPS
};

static GtkBuildableIface *parent_buildable_iface;
static GParamSpec *properties [N_PROPS];


static void
boards_dialog_add_child (GtkBuildable *buildable,
                         GtkBuilder   *builder,
                         GObject      *child,
                         const gchar  *type)
{
  BoardsDialog *self = (BoardsDialog *)buildable;

  g_assert (BOARDS_IS_DIALOG (self));

  if (boards_str_equal (type, "body"))
    boards_dialog_set_body (self, GTK_WIDGET (child));
  else if (boards_str_equal (type, "header-suffix"))
    boards_dialog_add_header_suffix (self, GTK_WIDGET (child));
  else if (boards_str_equal (type, "header-prefix"))
    boards_dialog_add_header_prefix (self, GTK_WIDGET (child));
  else
    parent_buildable_iface->add_child (buildable, builder, child, type);
}

static void
buildable_iface_init (GtkBuildableIface *iface)
{
  iface->add_child = boards_dialog_add_child;

  parent_buildable_iface = g_type_interface_peek_parent (iface);
}


static void
boards_dialog_dispose (GObject *object)
{
  BoardsDialog *self = (BoardsDialog *)object;

  g_clear_pointer ((GtkWidget **)&self->body, gtk_widget_unparent);
  g_clear_pointer ((GtkWidget **)&self->hbar, gtk_widget_unparent);
  g_clear_pointer ((GtkWidget **)&self->box, gtk_widget_unparent);

  G_OBJECT_CLASS (boards_dialog_parent_class)->dispose (object);
}

static void
boards_dialog_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  BoardsDialog *self = BOARDS_DIALOG (object);

  switch (prop_id)
    {
    case PROP_BODY:
      g_value_set_object (value, boards_dialog_get_body (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_dialog_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  BoardsDialog *self = BOARDS_DIALOG (object);

  switch (prop_id)
    {
    case PROP_BODY:
      boards_dialog_set_body (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static gboolean
boards_dialog_close_request (GtkWindow *window)
{
  gtk_window_destroy (window);
  return TRUE;
}

static void
boards_dialog_class_init (BoardsDialogClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWindowClass *window_class = GTK_WINDOW_CLASS (klass);

  object_class->dispose = boards_dialog_dispose;
  object_class->get_property = boards_dialog_get_property;
  object_class->set_property = boards_dialog_set_property;

  window_class->close_request = boards_dialog_close_request;

  properties [PROP_BODY] =
    g_param_spec_object ("body",
                         "Body",
                         "The GtkWidget that is the body.",
                         GTK_TYPE_WIDGET,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
boards_dialog_init (BoardsDialog *self)
{
  gtk_window_set_modal (GTK_WINDOW (self), TRUE);
  gtk_window_set_destroy_with_parent (GTK_WINDOW (self), TRUE);
  gtk_window_set_default_size (GTK_WINDOW (self), 400, 500);
  gtk_window_set_application (GTK_WINDOW (self),
                              GTK_APPLICATION (BOARDS_APPLICATION_DEFAULT));

  self->box = (GtkBox *)gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
  gtk_box_set_homogeneous (self->box, FALSE);

  self->hbar = (AdwHeaderBar *)adw_header_bar_new ();
  gtk_widget_add_css_class (GTK_WIDGET (self->hbar), "flat");
  gtk_box_append (self->box, GTK_WIDGET (self->hbar));

  self->body = (AdwBin *)adw_bin_new ();
  gtk_widget_set_hexpand (GTK_WIDGET (self->body), TRUE);
  gtk_widget_set_vexpand (GTK_WIDGET (self->body), TRUE);
  gtk_box_append (self->box, GTK_WIDGET (self->body));

  adw_window_set_content (ADW_WINDOW (self), GTK_WIDGET (self->box));
}


/**
 * boards_dialog_new:
 *
 * Create a new empty #BoardsDialog.
 *
 * Returns: a newly created #BoardsDialog.
 *
 * Since: 0.1
 */
BoardsDialog *
boards_dialog_new (void)
{
  return g_object_new (BOARDS_TYPE_DIALOG, NULL);
}

/**
 * boards_dialog_new_for_body:
 * @title: a #gchar: the title of the dialog.
 * @content: a #GtkWidget.
 *
 * Create a new #BoardsDialog and set @content as its body.
 *
 * Returns: a newly created #BoardsDialog.
 *
 * Since: 0.1
 */
BoardsDialog *
boards_dialog_new_for_content (const gchar *title,
                               GtkWidget   *content)
{
  g_return_val_if_fail (GTK_IS_WIDGET (content), NULL);

  return g_object_new (BOARDS_TYPE_DIALOG,
                       "title", title,
                       "body", content,
                       NULL);
}

GtkWidget *
boards_dialog_get_body (BoardsDialog *self)
{
  g_return_val_if_fail (BOARDS_IS_DIALOG (self), NULL);

  return adw_bin_get_child (self->body);
}

void
boards_dialog_set_body (BoardsDialog *self,
                        GtkWidget    *body)
{
  g_return_if_fail (BOARDS_IS_DIALOG (self));
  g_return_if_fail (GTK_IS_WIDGET (body));

  BOARDS_ENTRY;

  if (adw_bin_get_child (self->body) == body)
    BOARDS_EXIT;

  adw_bin_set_child (self->body, body);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_BODY]);

  BOARDS_EXIT;
}

void
boards_dialog_add_header_suffix (BoardsDialog *self,
                                 GtkWidget    *child)
{
  g_return_if_fail (BOARDS_IS_DIALOG (self));
  g_return_if_fail (GTK_IS_WIDGET (child));

  BOARDS_ENTRY;
  adw_header_bar_pack_end (self->hbar, child);
  BOARDS_EXIT;
}

void
boards_dialog_add_header_prefix (BoardsDialog *self,
                                 GtkWidget    *child)
{
  g_return_if_fail (BOARDS_IS_DIALOG (self));
  g_return_if_fail (GTK_IS_WIDGET (child));

  BOARDS_ENTRY;
  adw_header_bar_pack_start (self->hbar, child);
  BOARDS_EXIT;
}

void
boards_dialog_run (BoardsDialog *self)
{
  g_return_if_fail (BOARDS_IS_DIALOG (self));

  BOARDS_ENTRY;
  gtk_window_set_transient_for (GTK_WINDOW (self),
                                (GtkWindow *)boards_application_get_current_window (BOARDS_APPLICATION_DEFAULT));
  gtk_window_present (GTK_WINDOW (self));
  BOARDS_EXIT;
}


/* boards-page-info.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "boards-page-info"

#include "config.h"
#include "boards-debug.h"

#include "boards-macros.h"
#include "boards-page-info.h"

struct _BoardsPageInfo
{
  GtkPopover    parent_instance;

  BoardsPage *  page;

  /* -----< TEMPLATE WIDGET >----- */
  AdwEntryRow * page_title;
  AdwEntryRow * page_desc;
};

G_DEFINE_FINAL_TYPE (BoardsPageInfo, boards_page_info, GTK_TYPE_POPOVER)

enum {
  PROP_0,
  PROP_PAGE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


static void
boards_page_info_finalize (GObject *object)
{
  BoardsPageInfo *self = (BoardsPageInfo *)object;

  g_clear_weak_pointer (&self->page);

  G_OBJECT_CLASS (boards_page_info_parent_class)->finalize (object);
}

static void
boards_page_info_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  BoardsPageInfo *self = BOARDS_PAGE_INFO (object);

  switch (prop_id)
    {
    case PROP_PAGE:
      g_value_set_object (value, boards_page_info_get_page (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_page_info_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  BoardsPageInfo *self = BOARDS_PAGE_INFO (object);

  switch (prop_id)
    {
    case PROP_PAGE:
      boards_page_info_set_page (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_page_info_class_init (BoardsPageInfoClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = boards_page_info_finalize;
  object_class->get_property = boards_page_info_get_property;
  object_class->set_property = boards_page_info_set_property;

  properties [PROP_PAGE] =
    g_param_spec_object ("page",
                         "Page",
                         "The BoardsPage to link with.",
                         BOARDS_TYPE_PAGE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/ui/boards-page-info.ui");
  gtk_widget_class_bind_template_child (widget_class, BoardsPageInfo, page_title);
  gtk_widget_class_bind_template_child (widget_class, BoardsPageInfo, page_desc);
}

static void
boards_page_info_init (BoardsPageInfo *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}


/**
 * boards_page_info_new:
 *
 * Creates a new #BoardsPageInfo.
 *
 * Returns: a newly created #GtkPopover.
 *
 * Since: 0.1
 */
GtkPopover *
boards_page_info_new (void)
{
  return g_object_new (BOARDS_TYPE_PAGE_INFO, NULL);
}

BoardsPage *
boards_page_info_get_page (BoardsPageInfo *self)
{
  g_return_val_if_fail (BOARDS_IS_PAGE_INFO (self), NULL);

  return self->page;
}

void
boards_page_info_set_page (BoardsPageInfo *self,
                           BoardsPage     *page)
{
  g_return_if_fail (BOARDS_IS_PAGE_INFO (self));
  g_return_if_fail (!page || BOARDS_IS_PAGE (page));

  BOARDS_ENTRY;
  if (page == NULL)
    {
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PAGE]);
      BOARDS_EXIT;
    }

  if (self->page == page)
    BOARDS_EXIT;

  if (g_set_weak_pointer (&self->page, page))
    {
      g_object_bind_property (self->page, "title", self->page_title, "text", G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
      g_object_bind_property (self->page, "desc", self->page_desc, "text", G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PAGE]);
    }

  BOARDS_EXIT;
}

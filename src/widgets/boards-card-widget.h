/* boards-card-widget.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <adwaita.h>

#include "boards-card.h"
#include "boards-version-macros.h"

G_BEGIN_DECLS

#define BOARDS_TYPE_CARD_WIDGET (boards_card_widget_get_type())

BOARDS_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (BoardsCardWidget, boards_card_widget, BOARDS, CARD_WIDGET, GtkWidget)


BOARDS_AVAILABLE_IN_ALL
GtkWidget *  boards_card_widget_new       (void) G_GNUC_WARN_UNUSED_RESULT;

BOARDS_AVAILABLE_IN_ALL
BoardsCard * boards_card_widget_get_card  (BoardsCardWidget * self);
BOARDS_AVAILABLE_IN_ALL
void         boards_card_widget_set_card  (BoardsCardWidget * self,
                                           BoardsCard *       card);

G_END_DECLS

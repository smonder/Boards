/* boards-list-editor.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "boards-list-editor"

#include "config.h"
#include "boards-debug.h"

#include <glib/gi18n.h>

#include "boards-list-editor.h"

struct _BoardsListEditor
{
  GtkWidget       parent_instance;

  BoardsList *    list;

  AdwClamp *      body;
  AdwEntryRow *   list_title;
  AdwEntryRow *   list_desc;
  GtkSwitch *     list_completed;
};

G_DEFINE_FINAL_TYPE (BoardsListEditor, boards_list_editor, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_LIST,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


static void
boards_list_editor_finalize (GObject *object)
{
  BoardsListEditor *self = (BoardsListEditor *)object;

  g_clear_weak_pointer (&self->list);

  G_OBJECT_CLASS (boards_list_editor_parent_class)->finalize (object);
}

static void
boards_list_editor_dispose (GObject *object)
{
  BoardsListEditor *self = (BoardsListEditor *)object;

  g_clear_pointer ((GtkWidget **)&self->list_completed, gtk_widget_unparent);
  g_clear_pointer ((GtkWidget **)&self->list_desc, gtk_widget_unparent);
  g_clear_pointer ((GtkWidget **)&self->list_title, gtk_widget_unparent);
  g_clear_pointer ((GtkWidget **)&self->body, gtk_widget_unparent);

  G_OBJECT_CLASS (boards_list_editor_parent_class)->dispose (object);
}

static void
boards_list_editor_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  BoardsListEditor *self = BOARDS_LIST_EDITOR (object);

  switch (prop_id)
    {
    case PROP_LIST:
      g_value_set_object (value, boards_list_editor_get_list (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_list_editor_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  BoardsListEditor *self = BOARDS_LIST_EDITOR (object);

  switch (prop_id)
    {
    case PROP_LIST:
      boards_list_editor_set_list (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_list_editor_class_init (BoardsListEditorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = boards_list_editor_dispose;
  object_class->finalize = boards_list_editor_finalize;
  object_class->get_property = boards_list_editor_get_property;
  object_class->set_property = boards_list_editor_set_property;

  properties [PROP_LIST] =
    g_param_spec_object ("list",
                         "List",
                         "The BoardsList object to link with.",
                         BOARDS_TYPE_LIST,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_set_accessible_role (widget_class, GTK_ACCESSIBLE_ROLE_FORM);
  gtk_widget_class_set_template_from_resource (widget_class, "/ui/boards-list-editor.ui");

  gtk_widget_class_bind_template_child (widget_class, BoardsListEditor, body);
  gtk_widget_class_bind_template_child (widget_class, BoardsListEditor, list_title);
  gtk_widget_class_bind_template_child (widget_class, BoardsListEditor, list_desc);
  gtk_widget_class_bind_template_child (widget_class, BoardsListEditor, list_completed);
}

static void
boards_list_editor_init (BoardsListEditor *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}


/**
 * boards_list_editor_new:
 *
 * Creates a new #BoardsListEditor widget.
 *
 * Returns: a newly created #GtkWidget.
 *
 * Since: 0.1
 */
GtkWidget *
boards_list_editor_new (void)
{
  return g_object_new (BOARDS_TYPE_LIST_EDITOR, NULL);
}

BoardsList *
boards_list_editor_get_list (BoardsListEditor *self)
{
  g_return_val_if_fail (BOARDS_IS_LIST_EDITOR (self), NULL);

  return self->list;
}

void
boards_list_editor_set_list (BoardsListEditor *self,
                             BoardsList       *list)
{
  g_return_if_fail (BOARDS_IS_LIST_EDITOR (self));
  g_return_if_fail (BOARDS_IS_LIST (list));

  BOARDS_ENTRY;
  if (self->list == list)
    BOARDS_EXIT;

  if (g_set_weak_pointer (&self->list, list))
    {
      g_object_bind_property (self->list, "title",
                              self->list_title, "text",
                              G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
      g_object_bind_property (self->list, "desc",
                              self->list_desc, "text",
                              G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
      g_object_bind_property (self->list, "completed",
                              self->list_completed, "active",
                              G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_LIST]);
    }
  BOARDS_EXIT;
}

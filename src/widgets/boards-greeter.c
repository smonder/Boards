/* boards-greeter.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "boards-greeter"

#include "config.h"
#include "boards-debug.h"

#include <glib/gi18n.h>

#include "boards-page.h"
#include "boards-greeter.h"
#include "boards-macros.h"
#include "boards-page-view.h"
#include "boards-application.h"

struct _BoardsGreeter
{
  BoardsSurface   parent_instance;

  AdwStatusPage * status_page;
  GtkListBox *    recent_pages;
  GListModel *    recents_list;
};

G_DEFINE_FINAL_TYPE (BoardsGreeter, boards_greeter, BOARDS_TYPE_SURFACE)

enum {
  PROP_0,
  PROP_RECENTS_LIST,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


static void
on_new_page_btn_clicked_cb (BoardsGreeter *self,
                             GtkButton     *button)
{
  BoardsPage *new_page;

  g_assert (BOARDS_IS_GREETER (self));
  g_assert (GTK_IS_BUTTON (button));

  BOARDS_ENTRY;
  new_page = boards_page_new ();
  g_object_set (new_page,
                "name", "new_page",
                "title", "New Page",
                "desc", "A newly created page.",
                NULL);
  boards_page_set_modified (new_page, FALSE);
  boards_window_add_surface (boards_application_get_current_window (BOARDS_APPLICATION_DEFAULT),
                             boards_page_view_new (new_page));
  BOARDS_EXIT;
}

static void
on_open_page_dlg_response_cb (BoardsGreeter *self,
                              gint           response,
                              GtkDialog     *open_dlg)
{
  g_assert (BOARDS_IS_GREETER (self));
  g_assert (GTK_IS_DIALOG (open_dlg));

  BOARDS_ENTRY;
  if (response == GTK_RESPONSE_ACCEPT)
    {
      GtkFileChooser *chooser = GTK_FILE_CHOOSER (open_dlg);
      g_autoptr (GFile) page_file = gtk_file_chooser_get_file (chooser);
      g_autoptr (BoardsPage) page = boards_page_new ();
      const gchar * choice = gtk_file_chooser_get_choice (chooser, "encoding");
      GError *error = NULL;

      if (boards_str_equal (choice, "xmlfile"))
        {
          if (!boards_page_load_from_file (page, page_file, NULL, &error))
            {
              g_critical ("Error in loading page from file: %s", error->message);
              BOARDS_EXIT;
            }

          boards_window_add_surface (boards_application_get_current_window (BOARDS_APPLICATION_DEFAULT),
                                     boards_page_view_new (page));
        }
    }

  gtk_window_destroy (GTK_WINDOW (open_dlg));

  BOARDS_EXIT;
}

static void
on_open_file_btn_clicked_cb (BoardsGreeter *self,
                             GtkButton     *button)
{
  GtkWidget * open_page_dlg;
  g_autoptr (GtkFileFilter) file_filter = NULL;

  static const char *options[] = {
    "xmlfile",
    NULL
  };
  static const char *options_labels[] = {
    "XML File Encoding",
    NULL
  };

  g_assert (BOARDS_IS_GREETER (self));
  g_assert (GTK_IS_BUTTON (button));

  BOARDS_ENTRY;
  open_page_dlg = gtk_file_chooser_dialog_new (_("Open Page"),
                                               (GtkWindow *)boards_application_get_current_window (BOARDS_APPLICATION_DEFAULT),
                                               GTK_FILE_CHOOSER_ACTION_OPEN,
                                               _("_Cancel"), GTK_RESPONSE_CANCEL,
                                               _("_Open"), GTK_RESPONSE_ACCEPT,
                                               NULL);

  g_signal_connect_swapped (open_page_dlg, "response",
                            G_CALLBACK (on_open_page_dlg_response_cb), self);
  gtk_file_chooser_add_choice (GTK_FILE_CHOOSER (open_page_dlg),
                               "encoding", "File Enconding",
                               options, options_labels);

  file_filter = gtk_file_filter_new ();
  gtk_file_filter_set_name (file_filter, "Boards Page");
  gtk_file_filter_add_suffix (file_filter, "board");
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (open_page_dlg), file_filter);

  gtk_widget_show (open_page_dlg);
  BOARDS_EXIT;
}

static void
boards_greeter_dispose (GObject *object)
{
  BoardsGreeter *self = (BoardsGreeter *)object;

  g_clear_pointer ((GtkWidget **)&self->status_page, gtk_widget_unparent);
  g_clear_pointer ((GtkWidget **)&self->recent_pages, gtk_widget_unparent);

  G_OBJECT_CLASS (boards_greeter_parent_class)->dispose (object);
}

static void
boards_greeter_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  BoardsGreeter *self = BOARDS_GREETER (object);

  switch (prop_id)
    {
    case PROP_RECENTS_LIST:
      g_value_set_object (value, boards_greeter_get_recents_list (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_greeter_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  BoardsGreeter *self = BOARDS_GREETER (object);

  switch (prop_id)
    {
    case PROP_RECENTS_LIST:
      boards_greeter_bind_recents_list (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_greeter_class_init (BoardsGreeterClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = boards_greeter_dispose;
  object_class->get_property = boards_greeter_get_property;
  object_class->set_property = boards_greeter_set_property;

  /**
   * BoardsGreeter:recents-list:
   *
   * The "recents-list" property is the #GListModel of recent boards
   * that is going to be viewed in %self.
   *
   * Since: 0.1
   */
  properties [PROP_RECENTS_LIST] =
    g_param_spec_object ("recents-list",
                         "Recents List",
                         "The list of recent boards.",
                         G_TYPE_LIST_MODEL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/ui/boards-greeter.ui");
  gtk_widget_class_bind_template_child (widget_class, BoardsGreeter, status_page);
  gtk_widget_class_bind_template_child (widget_class, BoardsGreeter, recent_pages);
  gtk_widget_class_bind_template_callback (widget_class, on_new_page_btn_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_open_file_btn_clicked_cb);
}

static void
boards_greeter_init (BoardsGreeter *self)
{
  AdwActionRow * placeholder_row;
  gtk_widget_init_template (GTK_WIDGET (self));

  boards_surface_set_title (BOARDS_SURFACE (self), PACKAGE_NAME);
  boards_surface_set_icon_name (BOARDS_SURFACE (self), "io.sam.Boards-symbolic");

  g_object_bind_property (self, "title", self->status_page, "title", G_BINDING_SYNC_CREATE);

  placeholder_row = g_object_new (ADW_TYPE_ACTION_ROW,
                                  "subtitle", "No Recent Pages", NULL);
  gtk_list_box_set_placeholder (self->recent_pages, GTK_WIDGET (placeholder_row));
}


/**
 * boards_greeter_new:
 *
 * Creates a new #BoardsGreeter.
 *
 * Returns: A newly created #BoardsSurface.
 *
 * Since: 0.1
 */
BoardsSurface *
boards_greeter_new (const gchar *name)
{
  return g_object_new (BOARDS_TYPE_GREETER,
                       "name", name,
                       "title", "Welcome to "PACKAGE_NAME,
                       "icon-name", APP_ID"-symbolic",
                       NULL);
}

/**
 * boards_greeter_get_recents_list:
 * @self: a #BoardsGreeter.
 *
 * Gets the #GlistModel of recent boards bound to @sef.
 *
 * Returns: (transfer full) (nullable) a #GListModel.
 *
 * Since: 0.1
 */
GListModel *
boards_greeter_get_recents_list (BoardsGreeter *self)
{
  g_return_val_if_fail (BOARDS_IS_GREETER (self), NULL);

  return self->recents_list;
}

static void
on_row_activated_cb (BoardsGreeter *self,
                     AdwActionRow  *row)
{
  BoardsPage *item = g_object_get_data (G_OBJECT (row), "page");
  BoardsSurface *new_page;

  g_assert (BOARDS_IS_GREETER (self));
  g_assert (ADW_IS_ACTION_ROW (row));
  g_assert (BOARDS_IS_PAGE (item));

  BOARDS_ENTRY;
  new_page = boards_page_view_new (item);
  boards_window_add_surface (boards_application_get_current_window (BOARDS_APPLICATION_DEFAULT), new_page);
  BOARDS_EXIT;
}

static GtkWidget *
create_recents_row_func (BoardsPage   *item,
                         BoardsGreeter *self)
{
  AdwActionRow *row;

  g_assert (BOARDS_IS_GREETER (self));
  g_assert (BOARDS_IS_PAGE (item));

  row = g_object_new (ADW_TYPE_ACTION_ROW,
                      "title", boards_page_get_title (item),
                      "subtitle", boards_page_get_desc (item),
                      "icon-name", "view-paged-symbolic",
                      "subtitle-lines", 3,
                      "activatable", TRUE,
                      NULL);
  adw_action_row_add_suffix (row,
                             gtk_image_new_from_icon_name ("go-next-symbolic"));
  g_object_set_data (G_OBJECT (row), "page", item);
  g_signal_connect_swapped (row, "activated", G_CALLBACK (on_row_activated_cb), self);

  return (GtkWidget *)g_steal_pointer (&row);
}

/**
 * boards_greeter_bind_recents_list:
 * @self: a #BoardsGreeter.
 * @model: a #GListModel.
 *
 * Binds @model to @self's recent boards listbox.
 *
 * Since: 0.1
 */
void
boards_greeter_bind_recents_list (BoardsGreeter *self,
                                  GListModel    *model)
{
  g_return_if_fail (BOARDS_IS_GREETER (self));
  g_return_if_fail (G_IS_LIST_MODEL (model));

  if (self->recents_list == model)
    return;

  if (g_set_object (&self->recents_list, model))
    {
      gtk_list_box_bind_model (self->recent_pages,
                               self->recents_list,
                               (GtkListBoxCreateWidgetFunc)create_recents_row_func,
                               self, NULL);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_RECENTS_LIST]);
    }
}

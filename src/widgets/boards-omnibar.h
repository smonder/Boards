/* boards-omnibar.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <adwaita.h>

#include "boards-page.h"
#include "boards-version-macros.h"

G_BEGIN_DECLS

#define BOARDS_TYPE_OMNIBAR (boards_omnibar_get_type())

BOARDS_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (BoardsOmnibar, boards_omnibar, BOARDS, OMNIBAR, GtkWidget)

BOARDS_AVAILABLE_IN_ALL
GtkWidget *   boards_omnibar_new       (void) G_GNUC_WARN_UNUSED_RESULT;

BOARDS_AVAILABLE_IN_ALL
gchar *       boards_omnibar_get_title (BoardsOmnibar * self);
BOARDS_AVAILABLE_IN_ALL
void          boards_omnibar_set_title (BoardsOmnibar * self,
                                        const gchar *   title);

BOARDS_AVAILABLE_IN_ALL
BoardsPage *  boards_omnibar_get_page  (BoardsOmnibar * self);
BOARDS_AVAILABLE_IN_ALL
void          boards_omnibar_set_page  (BoardsOmnibar * self,
                                        BoardsPage *    page);


G_END_DECLS

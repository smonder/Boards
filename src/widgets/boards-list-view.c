/* boards-list-view.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "boards-list-view"

#include "config.h"
#include "boards-debug.h"

#include <glib/gi18n.h>

#include "boards-dialog.h"
#include "boards-list-editor.h"
#include "boards-list-view.h"
#include "boards-macros.h"
#include "boards-card.h"
#include "boards-card-widget.h"
#include "boards-page-view-private.h"


struct _BoardsListView
{
  GtkWidget       parent_instance;

  BoardsList *    list;
  GSequenceIter * iter;
  gint            x;
  gint            width;


  /* -----< TEMPLATE WIDGETS >----- */
  AdwPreferencesGroup * list_wrapper;
  GtkButton *           add_card;
  GtkButton *           edit_list;
  GtkButton *           remove_list;
  GtkButton *           move_list;
  GtkListBox *          cards_list;
};

typedef struct
{
  BoardsList *list;
  BoardsCard *card;
} DragData;

G_DEFINE_FINAL_TYPE (BoardsListView, boards_list_view, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_LIST,
  N_PROPS
};

enum {
  SIGNAL_REMOVE_LIST,
  N_SIGNALS
};

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];


static void
on_add_card_button_clicked_cb (BoardsListView *self,
                               GtkButton      *add_card)
{
  BoardsDialog *dialog;
  BoardsCardWidget *editor;
  BoardsCard *card;

  g_assert (BOARDS_IS_LIST_VIEW (self));
  g_assert (GTK_IS_BUTTON (add_card));

  BOARDS_ENTRY;
  card = boards_card_new ();
  boards_card_set_title (card, "Card");
  boards_card_set_desc (card, "card description.");
  boards_list_append_card (self->list, card);

  editor = (BoardsCardWidget *)boards_card_widget_new ();
  gtk_widget_set_hexpand (GTK_WIDGET (editor), TRUE);
  gtk_widget_set_vexpand (GTK_WIDGET (editor), TRUE);
  boards_card_widget_set_card (editor, card);

  dialog = boards_dialog_new_for_content (boards_card_get_title (card),
                                          GTK_WIDGET (editor));
  g_object_bind_property (card, "title", dialog, "title", G_BINDING_SYNC_CREATE);
  boards_dialog_run (dialog);
  BOARDS_EXIT;
}

static void
on_edit_list_button_clicked_cb (BoardsListView *self,
                                GtkButton      *edit_list)
{
  BoardsDialog *dialog;
  BoardsListEditor *editor;

  g_assert (BOARDS_IS_LIST_VIEW (self));
  g_assert (GTK_IS_BUTTON (edit_list));

  BOARDS_ENTRY;
  editor = (BoardsListEditor *)boards_list_editor_new ();
  gtk_widget_set_hexpand (GTK_WIDGET (editor), TRUE);
  gtk_widget_set_vexpand (GTK_WIDGET (editor), TRUE);
  boards_list_editor_set_list (editor, self->list);

  dialog = boards_dialog_new_for_content (boards_list_get_title (self->list),
                                          GTK_WIDGET (editor));
  g_object_bind_property (self->list, "title", dialog, "title", G_BINDING_SYNC_CREATE);
  boards_dialog_run (dialog);
  BOARDS_EXIT;
}

static void
on_remove_list_button_clicked_cb (BoardsListView *self,
                                  GtkButton      *remove_list)
{
  g_assert (BOARDS_IS_LIST_VIEW (self));
  g_assert (GTK_IS_BUTTON (remove_list));

  g_signal_emit (self, signals [SIGNAL_REMOVE_LIST], 0);
}

static gboolean
on_boards_list_view_drop_cb (GtkDropTarget *target,
                             const GValue  *value,
                             double         x,
                             double         y,
                             gpointer       user_data)
{
  BoardsListView *self = (BoardsListView *)user_data;
  guint position;

  g_assert (BOARDS_IS_LIST_VIEW (self));

  /* Call the appropriate setter depending on the type of data
   * that we received
   */
  if (!G_VALUE_HOLDS (value, BOARDS_TYPE_CARD))
    return FALSE;

  position = gtk_list_box_row_get_index (gtk_list_box_get_row_at_y (self->cards_list, y));

  if (boards_list_is_empty (self->list) ||
      position >= boards_list_get_n_cards (self->list))
    {
      boards_list_append_card (self->list, g_value_get_object (value));
      return TRUE;
    }

  boards_list_insert_card (self->list, g_value_get_object (value), position);

  return TRUE;
}

static GdkDragAction
on_boards_list_view_motion_cb (GtkDropTarget *target,
                               double         x,
                               double         y,
                               gpointer       user_data)
{
  BoardsListView *self = (BoardsListView *)user_data;

  g_assert (BOARDS_IS_LIST_VIEW (self));

  gtk_list_box_drag_highlight_row (self->cards_list,
                                   gtk_list_box_get_row_at_y (self->cards_list, y));

  return GDK_ACTION_MOVE;
}

static void
on_boards_list_view_leave_cb (GtkDropTarget *target,
                              gpointer       user_data)
{
  BoardsListView *self = (BoardsListView *)user_data;

  g_assert (BOARDS_IS_LIST_VIEW (self));

  gtk_list_box_drag_unhighlight_row (self->cards_list);
}

static void
boards_list_view_dispose (GObject *object)
{
  BoardsListView *self = (BoardsListView *)object;
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (self))) != NULL)
    g_clear_pointer (&child, gtk_widget_unparent);

  G_OBJECT_CLASS (boards_list_view_parent_class)->dispose (object);
}

static void
boards_list_view_finalize (GObject *object)
{
  BoardsListView *self = (BoardsListView *)object;

  g_clear_object (&self->list);

  G_OBJECT_CLASS (boards_list_view_parent_class)->finalize (object);
}

static void
boards_list_view_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  BoardsListView *self = BOARDS_LIST_VIEW (object);

  switch (prop_id)
    {
    case PROP_LIST:
      g_value_set_object (value, boards_list_view_get_list (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_list_view_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  BoardsListView *self = BOARDS_LIST_VIEW (object);

  switch (prop_id)
    {
    case PROP_LIST:
      boards_list_view_set_list (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_list_view_class_init (BoardsListViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = boards_list_view_dispose;
  object_class->finalize = boards_list_view_finalize;
  object_class->get_property = boards_list_view_get_property;
  object_class->set_property = boards_list_view_set_property;

  /**
   * BoardsListView:list:
   *
   * The "list" property is the #BoardsList that this wodget wraps.
   *
   * Since: 0.1
   */
  properties [PROP_LIST] =
    g_param_spec_object ("list",
                         "List",
                         "The BoardsList connected to this widget.",
                         BOARDS_TYPE_LIST,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  /**
   * BoardsListView::remove-list:
   *
   * The "remove-list" signal is emitted when the remove button is clicked.
   * The handler should deal with the deletion of the #BoardsList linked
   * with %self from its #BoardsPage.
   *
   * Since: 0.1
   */
  signals [SIGNAL_REMOVE_LIST] =
    g_signal_new ("remove-list",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0, NULL, NULL, NULL,
                  G_TYPE_NONE, 0);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_set_accessible_role (widget_class, GTK_ACCESSIBLE_ROLE_GROUP);
  gtk_widget_class_set_template_from_resource (widget_class, "/ui/boards-list-view.ui");
  gtk_widget_class_bind_template_child (widget_class, BoardsListView, list_wrapper);
  gtk_widget_class_bind_template_child (widget_class, BoardsListView, add_card);
  gtk_widget_class_bind_template_child (widget_class, BoardsListView, edit_list);
  gtk_widget_class_bind_template_child (widget_class, BoardsListView, remove_list);
  gtk_widget_class_bind_template_child (widget_class, BoardsListView, move_list);
  gtk_widget_class_bind_template_child (widget_class, BoardsListView, cards_list);
  gtk_widget_class_bind_template_callback (widget_class, on_add_card_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_edit_list_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_remove_list_button_clicked_cb);

  g_type_ensure (BOARDS_TYPE_CARD_WIDGET);
}

static void
boards_list_view_init (BoardsListView *self)
{
  GtkDropTarget *drop_target;

  gtk_widget_init_template (GTK_WIDGET (self));

  drop_target = gtk_drop_target_new (BOARDS_TYPE_CARD, GDK_ACTION_MOVE);
  gtk_event_controller_set_propagation_phase (GTK_EVENT_CONTROLLER (drop_target), GTK_PHASE_BUBBLE);
  gtk_drop_target_set_gtypes (drop_target, (GType [1]) { BOARDS_TYPE_CARD }, 1);
  g_signal_connect (drop_target, "drop", G_CALLBACK (on_boards_list_view_drop_cb), self);
  g_signal_connect (drop_target, "motion", G_CALLBACK (on_boards_list_view_motion_cb), self);
  g_signal_connect (drop_target, "leave", G_CALLBACK (on_boards_list_view_leave_cb), self);
  gtk_widget_add_controller (GTK_WIDGET (self->cards_list),
                             GTK_EVENT_CONTROLLER (drop_target));
}


/**
 * boards_list_view_new:
 *
 * Creates a new #BoardsListView widget.
 *
 * Returns: a newly created #GtkWidget.
 *
 * Since: 0.1
 */
GtkWidget *
boards_list_view_new (void)
{
  return g_object_new (BOARDS_TYPE_LIST_VIEW, NULL);
}

/**
 * boards_list_view_get_list:
 * @self: a #BoardsListView.
 *
 * Gets the "list" property of @self.
 *
 * Returns: (transfer full): a #BoardsList.
 *
 * Since: 0.1
 */
BoardsList *
boards_list_view_get_list (BoardsListView *self)
{
  g_return_val_if_fail (BOARDS_IS_LIST_VIEW (self), NULL);

  return self->list;
}

static GdkContentProvider *
on_drag_prepare (GtkDragSource *source,
                 double         x,
                 double         y,
                 AdwActionRow  *row)
{
  BoardsCard *card = g_object_get_data (G_OBJECT (row), "card");

  g_assert (ADW_IS_ACTION_ROW (row));
  g_assert (BOARDS_IS_CARD (card));

  return gdk_content_provider_new_union ((GdkContentProvider *[1]) {
      gdk_content_provider_new_typed (BOARDS_TYPE_CARD, card),
    }, 1);
}

static void
on_drag_begin (GtkDragSource *source,
               GdkDrag       *drag,
               AdwActionRow  *row)
{
  GdkPaintable *paintable;
  g_assert (ADW_IS_ACTION_ROW (row));

  /* Set the widget as the drag icon */
  paintable = gtk_widget_paintable_new (GTK_WIDGET (row));
  gtk_drag_source_set_icon (source, paintable, 0, 0);
  g_object_unref (paintable);
}

static void
on_drag_end (GtkDragSource *source,
             GdkDrag       *drag,
             gboolean       delete_data,
             gpointer       user_data)
{
  DragData *data = user_data;

  g_assert (BOARDS_IS_LIST (data->list));
  g_assert (BOARDS_IS_CARD (data->card));

  if (delete_data)
    {
      boards_list_delete_card (data->list, data->card);
    }
}

static void
on_delete_card_button_clicked_cb (GtkButton *btn,
                                  gpointer   user_data)
{
  DragData *data = user_data;

  g_assert (GTK_IS_BUTTON (btn));
  g_assert (BOARDS_IS_LIST (data->list));
  g_assert (BOARDS_IS_CARD (data->card));

  boards_list_delete_card (data->list, data->card);
  boards_dialog_destroy (g_object_get_data (G_OBJECT (btn), "dialog"));
}

static void
on_card_row_activated_cb (BoardsListView   *self,
                          AdwActionRow *card_row)
{
  BoardsDialog *dialog;
  GtkButton *delete_button;
  BoardsCardWidget *editor;
  BoardsCard *card = g_object_get_data (G_OBJECT (card_row), "card");
  DragData *data = g_new0 (DragData, 1);

  g_assert (BOARDS_IS_LIST_VIEW (self));
  g_assert (ADW_IS_ACTION_ROW (card_row));

  BOARDS_ENTRY;
  editor = (BoardsCardWidget *)boards_card_widget_new ();
  gtk_widget_set_hexpand (GTK_WIDGET (editor), TRUE);
  gtk_widget_set_vexpand (GTK_WIDGET (editor), TRUE);
  boards_card_widget_set_card (editor, card);

  delete_button = (GtkButton *)gtk_button_new ();
  gtk_button_set_icon_name (delete_button, "user-trash-symbolic");
  gtk_widget_set_tooltip_text (GTK_WIDGET (delete_button), _("Remove card from the list"));
  gtk_widget_add_css_class (GTK_WIDGET (delete_button), "destructive-action");

  dialog = boards_dialog_new_for_content (boards_card_get_title (card),
                                          GTK_WIDGET (editor));
  boards_dialog_add_header_suffix (dialog, GTK_WIDGET (delete_button));
  g_object_bind_property (card, "title", dialog, "title", G_BINDING_SYNC_CREATE);

  data->list = boards_list_view_get_list (self);
  data->card = card;
  g_object_set_data (G_OBJECT (delete_button), "dialog", dialog);
  g_signal_connect (delete_button, "clicked",
                    G_CALLBACK (on_delete_card_button_clicked_cb), data);

  boards_dialog_run (dialog);
  BOARDS_EXIT;
}

static void
on_card_notify_state_cb (AdwActionRow *row,
                         GParamSpec   *pspec,
                         BoardsCard   *card)
{
  g_assert (BOARDS_IS_CARD (card));
  g_assert (ADW_IS_ACTION_ROW (row));

  switch (boards_card_get_state (card))
    {
    case BOARDS_STATE_NORMAL:
      gtk_widget_remove_css_class (GTK_WIDGET (row), "card_completed");
      gtk_widget_remove_css_class (GTK_WIDGET (row), "card_postponed");
      gtk_widget_remove_css_class (GTK_WIDGET (row), "card_working");
      break;

    case BOARDS_STATE_COMPLETED:
      gtk_widget_add_css_class (GTK_WIDGET (row), "card_completed");
      gtk_widget_remove_css_class (GTK_WIDGET (row), "card_postponed");
      gtk_widget_remove_css_class (GTK_WIDGET (row), "card_working");
      break;

    case BOARDS_STATE_WORKING:
      gtk_widget_remove_css_class (GTK_WIDGET (row), "card_completed");
      gtk_widget_remove_css_class (GTK_WIDGET (row), "card_postponed");
      gtk_widget_add_css_class (GTK_WIDGET (row), "card_working");
      break;

    case BOARDS_STATE_POSTPONED:
      gtk_widget_remove_css_class (GTK_WIDGET (row), "card_completed");
      gtk_widget_add_css_class (GTK_WIDGET (row), "card_postponed");
      gtk_widget_remove_css_class (GTK_WIDGET (row), "card_working");
      break;

    case BOARDS_STATE_INVALID: break;
    default: break;
    }
}

static GtkWidget *
create_widget_func (gpointer item,
                    gpointer user_data)
{
  BoardsListView *self = user_data;
  BoardsCard *card = (BoardsCard *)item;
  GtkWidget * row;
  GtkDragSource *drag_source;
  DragData *data = g_new0 (DragData, 1);

  g_assert (BOARDS_IS_LIST_VIEW (self));
  g_assert (BOARDS_IS_CARD (card));

  BOARDS_ENTRY;

  row = adw_action_row_new ();
  adw_action_row_set_title_lines (ADW_ACTION_ROW (row), 3);
  g_object_bind_property (card, "title", row, "title", G_BINDING_SYNC_CREATE);
  gtk_list_box_row_set_activatable (GTK_LIST_BOX_ROW (row), TRUE);

  g_object_set_data (G_OBJECT (row), "card", card);
  switch (boards_card_get_state (card))
    {
    case BOARDS_STATE_COMPLETED:
      gtk_widget_add_css_class (GTK_WIDGET (row), "card_completed");
      break;

    case BOARDS_STATE_WORKING:
      gtk_widget_add_css_class (GTK_WIDGET (row), "card_working");
      break;

    case BOARDS_STATE_POSTPONED:
      gtk_widget_add_css_class (GTK_WIDGET (row), "card_postponed");
      break;

    case BOARDS_STATE_NORMAL: break;
    case BOARDS_STATE_INVALID: break;
    default: break;
    }

  g_signal_connect_swapped (row, "activated", G_CALLBACK (on_card_row_activated_cb), self);
  g_signal_connect_swapped (card, "notify::state", G_CALLBACK (on_card_notify_state_cb), row);

  data->list = self->list;
  data->card = card;

  drag_source = gtk_drag_source_new ();
  gtk_event_controller_set_propagation_phase (GTK_EVENT_CONTROLLER (drag_source), GTK_PHASE_BUBBLE);
  gtk_drag_source_set_actions (drag_source, GDK_ACTION_MOVE);
  g_signal_connect (drag_source, "prepare", G_CALLBACK (on_drag_prepare), row);
  g_signal_connect (drag_source, "drag-begin", G_CALLBACK (on_drag_begin), row);
  g_signal_connect (drag_source, "drag-end", G_CALLBACK (on_drag_end), data);
  gtk_widget_add_controller (row, GTK_EVENT_CONTROLLER (drag_source));

  BOARDS_RETURN (row);
}

/**
 * boards_list_view_set_list:
 * @self: a #BoardsListView.
 * @list: a #BoardsList.
 *
 * Sets the "list" property of @self.
 *
 * Since: 0.1
 */
void
boards_list_view_set_list (BoardsListView *self,
                           BoardsList    *list)
{
  g_return_if_fail (BOARDS_IS_LIST_VIEW (self));
  g_return_if_fail (BOARDS_IS_LIST (list));

  BOARDS_ENTRY;

  if (self->list == list)
    return;

  if (g_set_object (&self->list, list))
    {
      g_object_bind_property (self->list, "title",
                              self->list_wrapper, "title",
                              G_BINDING_SYNC_CREATE);
      g_object_bind_property (self->list, "desc",
                              self->list_wrapper, "description",
                              G_BINDING_SYNC_CREATE);

      gtk_list_box_bind_model (self->cards_list,
                               boards_list_get_cards (self->list),
                               (GtkListBoxCreateWidgetFunc) create_widget_func,
                               self, NULL);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_LIST]);
    }

  BOARDS_EXIT;
}

void
_boards_list_view_set_iter (BoardsListView *list_view,
                            GSequenceIter  *iter)
{
  g_return_if_fail (BOARDS_IS_LIST_VIEW (list_view));

  list_view->iter = iter;
}

GSequenceIter *
_boards_list_view_get_iter (BoardsListView *list_view)
{
  g_return_val_if_fail (BOARDS_IS_LIST_VIEW (list_view), NULL);

  return list_view->iter;
}

gint
_boards_list_view_get_index (BoardsListView *list_view)
{
  g_return_val_if_fail (BOARDS_IS_LIST_VIEW (list_view), -1);

  if (list_view->iter != NULL)
    return g_sequence_iter_get_position (list_view->iter);

  return -1;
}

GtkWidget *
_boards_list_view_get_drag_widget (BoardsListView *list_view)
{
  g_return_val_if_fail (BOARDS_IS_LIST_VIEW (list_view), NULL);

  return (GtkWidget *)list_view->move_list;
}

gint
_boards_list_view_get_x (BoardsListView *list_view)
{
  g_return_val_if_fail (BOARDS_IS_LIST_VIEW (list_view), 0);
  return list_view->x;
}

void
_boards_list_view_set_x (BoardsListView *list_view,
                         const gint      x)
{
  g_return_if_fail (list_view);
  list_view->x = x;
}

gint
_boards_list_view_get_width (BoardsListView *list_view)
{
  g_return_val_if_fail (BOARDS_IS_LIST_VIEW (list_view), 0);
  return list_view->width;
}

void
_boards_list_view_set_width (BoardsListView *list_view,
                             const gint      width)
{
  g_return_if_fail (BOARDS_IS_LIST_VIEW (list_view));
  list_view->width = width;
}


/* boards-omnibar.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "boards-omnibar"

#include "config.h"
#include "boards-debug.h"

#include <libpanel.h>
#include <glib/gi18n.h>

#include "boards-application.h"
#include "boards-dialog.h"
#include "boards-list-editor.h"
#include "boards-macros.h"
#include "boards-omnibar.h"
#include "boards-page-info.h"


struct _BoardsOmnibar
{
  GtkWidget        parent_instance;

  BoardsPage *     page;

  /* -----< TEMPLATE WIDGETS >----- */
  PanelOmniBar *   panel_omnibar;
  GtkLabel *       title;
  BoardsPageInfo * page_info;
  GtkButton *      new_list;
};

G_DEFINE_FINAL_TYPE (BoardsOmnibar, boards_omnibar, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_TITLE,
  PROP_PAGE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


static void
on_new_list_button_clicked_cb (BoardsOmnibar *self,
                               GtkButton     *btn)
{
  g_autoptr (BoardsList) new_list = NULL;
  BoardsDialog *dialog;
  BoardsListEditor *editor;

  g_assert (BOARDS_IS_OMNIBAR (self));
  g_assert (GTK_IS_BUTTON (btn));

  BOARDS_ENTRY;
  if (!self->page)
    BOARDS_EXIT;

  new_list = boards_list_new ();
  boards_list_set_title (new_list, "dummy");
  boards_list_set_desc (new_list, "dummy");
  boards_page_append_list (self->page, new_list);

  editor = (BoardsListEditor *)boards_list_editor_new ();
  gtk_widget_set_hexpand (GTK_WIDGET (editor), TRUE);
  gtk_widget_set_vexpand (GTK_WIDGET (editor), TRUE);
  boards_list_editor_set_list (editor, new_list);

  dialog = boards_dialog_new_for_content (boards_list_get_title (new_list),
                                          GTK_WIDGET (editor));
  g_object_bind_property (new_list, "title", dialog, "title", G_BINDING_SYNC_CREATE);
  boards_dialog_run (dialog);
  BOARDS_EXIT;
}

static void
on_save_page_dlg_response_cb (BoardsOmnibar *self,
                              gint           response,
                              GtkDialog     *save_dlg)
{
  g_assert (BOARDS_IS_OMNIBAR (self));
  g_assert (GTK_IS_DIALOG (save_dlg));

  BOARDS_ENTRY;
  if (response == GTK_RESPONSE_ACCEPT)
    {
      GtkFileChooser *chooser = GTK_FILE_CHOOSER (save_dlg);
      g_autoptr (GFile) page_file = gtk_file_chooser_get_file (chooser);
      const gchar * choice = gtk_file_chooser_get_choice (chooser, "encoding");
      GError *error = NULL;

      if (boards_str_equal (choice, "xmlfile"))
        {
          if (!boards_page_save_to_file (self->page, page_file, NULL, &error))
            g_critical ("Error saving page: %s", error->message);
          else
            boards_page_set_file (self->page, g_steal_pointer (&page_file));
        }
    }

  gtk_window_destroy (GTK_WINDOW (save_dlg));
  BOARDS_EXIT;
}

static void
on_save_button_clicked_cb (BoardsOmnibar *self,
                           GtkButton     *save_btn)
{
  GFile * file = NULL;
  GtkWidget *save_page_dlg;
  g_autoptr (GtkFileFilter) file_filter = NULL;
  static const char *options[] = { "xmlfile", NULL };
  static const char *options_labels[] = { "XML File Encoding", NULL };
  GError *error = NULL;

  g_assert (BOARDS_IS_OMNIBAR (self));
  g_assert (GTK_IS_BUTTON (save_btn));

  BOARDS_ENTRY;
  if (!self->page)
    BOARDS_EXIT;

  file = boards_page_get_file (self->page);

  if (!file)
    {
      save_page_dlg = gtk_file_chooser_dialog_new (_("Save Page"),
                                                   (GtkWindow *)boards_application_get_current_window (BOARDS_APPLICATION_DEFAULT),
                                                   GTK_FILE_CHOOSER_ACTION_SAVE,
                                                   _("_Cancel"), GTK_RESPONSE_CANCEL,
                                                   _("_Save"), GTK_RESPONSE_ACCEPT,
                                                   NULL);

      g_signal_connect_swapped (save_page_dlg, "response",
                                G_CALLBACK (on_save_page_dlg_response_cb), self);
      gtk_file_chooser_add_choice (GTK_FILE_CHOOSER (save_page_dlg),
                                   "encoding", "File Enconding",
                                   options, options_labels);

      file_filter = gtk_file_filter_new ();
      gtk_file_filter_set_name (file_filter, "Boards Page");
      gtk_file_filter_add_suffix (file_filter, "board");
      gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (save_page_dlg), file_filter);

      gtk_widget_show (save_page_dlg);
      BOARDS_EXIT;
    }

  if (!boards_page_save_to_file (self->page,
                                 boards_page_get_file (self->page),
                                 NULL, &error))
    g_critical ("Error saving page: %s", error->message);

  BOARDS_EXIT;
}

static void
boards_omnibar_dispose (GObject *object)
{
  BoardsOmnibar *self = (BoardsOmnibar *)object;
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (self))) != NULL)
    g_clear_pointer (&child, gtk_widget_unparent);

  G_OBJECT_CLASS (boards_omnibar_parent_class)->dispose (object);
}

static void
boards_omnibar_finalize (GObject *object)
{
  BoardsOmnibar *self = (BoardsOmnibar *)object;

  g_clear_weak_pointer (&self->page);

  G_OBJECT_CLASS (boards_omnibar_parent_class)->finalize (object);
}

static void
boards_omnibar_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  BoardsOmnibar *self = BOARDS_OMNIBAR (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      g_value_take_string (value, boards_omnibar_get_title (self));
      break;

    case PROP_PAGE:
      g_value_set_object (value, boards_omnibar_get_page (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_omnibar_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  BoardsOmnibar *self = BOARDS_OMNIBAR (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      boards_omnibar_set_title (self, g_value_get_string (value));
      break;

    case PROP_PAGE:
      boards_omnibar_set_page (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
boards_omnibar_class_init (BoardsOmnibarClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = boards_omnibar_dispose;
  object_class->finalize = boards_omnibar_finalize;
  object_class->get_property = boards_omnibar_get_property;
  object_class->set_property = boards_omnibar_set_property;

  properties [PROP_TITLE] =
    g_param_spec_string ("title",
                         "Title",
                         "The title of the omnibar.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_PAGE] =
    g_param_spec_object ("page",
                         "Page",
                         "The BoardsPage to link with.",
                         BOARDS_TYPE_PAGE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  g_type_ensure (BOARDS_TYPE_PAGE_INFO);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_set_accessible_role (widget_class, GTK_ACCESSIBLE_ROLE_GROUP);

  gtk_widget_class_set_template_from_resource (widget_class, "/ui/boards-omnibar.ui");
  gtk_widget_class_bind_template_child (widget_class, BoardsOmnibar, panel_omnibar);
  gtk_widget_class_bind_template_child (widget_class, BoardsOmnibar, title);
  gtk_widget_class_bind_template_child (widget_class, BoardsOmnibar, page_info);
  gtk_widget_class_bind_template_child (widget_class, BoardsOmnibar, new_list);
  gtk_widget_class_bind_template_callback (widget_class, on_new_list_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_save_button_clicked_cb);
}

static void
boards_omnibar_init (BoardsOmnibar *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}


/**
 * boards_omnibar_new:
 *
 * Creates a new #BoardsOmnibar.
 *
 * Returns: a newly created #GtkWidget.
 *
 * Since: 0.1
 */
GtkWidget *
boards_omnibar_new (void)
{
  return g_object_new (BOARDS_TYPE_OMNIBAR, NULL);
}


gchar *
boards_omnibar_get_title (BoardsOmnibar *self)
{
  g_return_val_if_fail (BOARDS_IS_OMNIBAR (self), NULL);

  return g_strdup (gtk_label_get_label (self->title));
}

void
boards_omnibar_set_title (BoardsOmnibar *self,
                          const gchar   *title)
{
  g_return_if_fail (BOARDS_IS_OMNIBAR (self));

  BOARDS_ENTRY;
  if (!boards_str_equal (gtk_label_get_label (self->title), title))
    {
      gtk_label_set_label (self->title, title);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TITLE]);
    }
  BOARDS_EXIT;
}

BoardsPage *
boards_omnibar_get_page (BoardsOmnibar *self)
{
  g_return_val_if_fail (BOARDS_IS_OMNIBAR (self), NULL);

  return self->page;
}

void
boards_omnibar_set_page (BoardsOmnibar *self,
                          BoardsPage   *page)
{
  g_return_if_fail (BOARDS_IS_OMNIBAR (self));
  g_return_if_fail (!page || BOARDS_IS_PAGE (page));

  BOARDS_ENTRY;
  if (!page)
    {
      g_clear_weak_pointer (&self->page);
      boards_page_info_set_page (self->page_info, NULL);
      panel_omni_bar_set_popover (self->panel_omnibar, NULL);
      gtk_widget_set_visible (GTK_WIDGET (self->new_list), FALSE);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PAGE]);
      BOARDS_EXIT;
    }

  if (self->page == page)
    BOARDS_EXIT;

  if (g_set_weak_pointer (&self->page, page))
    {
      boards_page_info_set_page (self->page_info, self->page);
      panel_omni_bar_set_popover (self->panel_omnibar, GTK_POPOVER (self->page_info));
      gtk_widget_set_visible (GTK_WIDGET (self->new_list), TRUE);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PAGE]);
    }
  BOARDS_EXIT;
}


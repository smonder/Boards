/* boards-page-view-private.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <glib/gi18n.h>

#include "boards-list-view.h"
#include "boards-macros.h"
#include "boards-page-view.h"

struct _BoardsPageView
{
  BoardsSurface    parent_instance;

  BoardsPage *     page;

  GtkBox *         lists_box;
  GSequence *      lists;

  BoardsListView * highlighted_list;
};

typedef struct
{
  BoardsPageView *page_view;
  BoardsListView *list_view;
} PageViewDragData;


void             _boards_page_view_queue_save        (BoardsPageView * self);

void             _boards_page_view_highlight_list    (BoardsPageView * self,
                                                      BoardsListView * list);

void             _boards_page_view_unhighlight_list  (BoardsPageView * self);

BoardsListView * _boards_page_view_get_list_at_index (BoardsPageView * self,
                                                      gint             index_);

BoardsListView * _boards_page_view_get_list_at_x     (BoardsPageView * self,
                                                      gint             x);

void             _boards_page_view_remove_list       (BoardsPageView * self,
                                                      BoardsListView * list_view);

void             _boards_page_view_insert_list       (BoardsPageView * self,
                                                      BoardsListView * list_view,
                                                      const gint       position);

void             _boards_page_view_queue_update      (BoardsPageView * self);



GtkWidget *      _boards_list_view_get_drag_widget   (BoardsListView * list_view);
void             _boards_list_view_set_iter          (BoardsListView * list_view,
                                                      GSequenceIter *  iter);
GSequenceIter *  _boards_list_view_get_iter          (BoardsListView * list_view);
gint             _boards_list_view_get_index         (BoardsListView * list_view);

gint             _boards_list_view_get_x             (BoardsListView * list_view);
void             _boards_list_view_set_x             (BoardsListView * list_view,
                                                      const gint       x);

gint             _boards_list_view_get_width         (BoardsListView * list_view);
void             _boards_list_view_set_width         (BoardsListView * list_view,
                                                      const gint       width);


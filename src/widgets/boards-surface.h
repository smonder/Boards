/* boards-surface.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <adwaita.h>

#include "boards-version-macros.h"

G_BEGIN_DECLS

#define BOARDS_TYPE_SURFACE (boards_surface_get_type())

BOARDS_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (BoardsSurface, boards_surface, BOARDS, SURFACE, GtkWidget)

struct _BoardsSurfaceClass
{
  GtkWidgetClass parent_class;

  /* -----< SIGNALS >----- */
  void           (* surface_set)   (BoardsSurface * surface);
  void           (* surface_unset) (BoardsSurface * surface);

  /* -----< PRIVATE >----- */
  gpointer       padding [8];
};


BOARDS_AVAILABLE_IN_ALL
gchar *        boards_surface_get_name       (BoardsSurface * self);

BOARDS_AVAILABLE_IN_ALL
gchar *        boards_surface_get_title      (BoardsSurface * self);
BOARDS_AVAILABLE_IN_ALL
void           boards_surface_set_title      (BoardsSurface * self,
                                              const gchar *   title);

BOARDS_AVAILABLE_IN_ALL
gchar *        boards_surface_get_icon_name  (BoardsSurface * self);
BOARDS_AVAILABLE_IN_ALL
void           boards_surface_set_icon_name  (BoardsSurface * self,
                                              const gchar *   icon_name);

BOARDS_AVAILABLE_IN_ALL
GtkWidget *    boards_surface_get_child      (BoardsSurface * self);
BOARDS_AVAILABLE_IN_ALL
void           boards_surface_set_child      (BoardsSurface * self,
                                              GtkWidget *     child);

BOARDS_AVAILABLE_IN_ALL
GMenuModel *   boards_surface_get_rc_menu    (BoardsSurface * self);
BOARDS_AVAILABLE_IN_ALL
void           boards_surface_set_rc_menu    (BoardsSurface * self,
                                              GMenuModel *    menu);

BOARDS_AVAILABLE_IN_ALL
void           boards_surface_set            (BoardsSurface * self);
BOARDS_AVAILABLE_IN_ALL
void           boards_surface_unset          (BoardsSurface * self);

G_END_DECLS
